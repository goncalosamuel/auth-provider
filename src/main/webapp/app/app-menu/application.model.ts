export class Application {
    public applicationId: number;
    public applicationName: string;
    public applicationDescription: string;
    public applicationUrl: string;
    public active: boolean;


    constructor() {}

}