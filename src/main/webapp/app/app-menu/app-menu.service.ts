import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Application } from './application.model';
import { PagedResponse } from '../common/model/page-response.model';

@Injectable()
export class AppMenuService {

    constructor(private http: HttpClient) {}

    getApps(): Promise<PagedResponse<Application>> {
        return new Promise((resolve, reject) => {
            this.http.get<PagedResponse<Application>>('application/').subscribe((apps) => {
                resolve(apps);
            }, (err) => {
                reject(err);
            })
        })
    }

}
