import { HHCommonModule } from '../common';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { AppMenuComponent } from './app-menu.component';
import { AppMenuService } from './app-menu.service';

@NgModule({
    imports: [
        HHCommonModule,
        FormsModule
    ],
    declarations: [
        AppMenuComponent
    ],
    providers: [
        AppMenuService
    ]
})
export class AppMenuModule { }
