import { OnInit, Component } from '@angular/core';
import { AppMenuService } from './app-menu.service';
import { Application } from './application.model';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastsManager } from 'ng2-toastr';

@Component({
    templateUrl: './app-menu.html'
})
export class AppMenuComponent implements OnInit {

    public apps: Application[];
    constructor(private service: AppMenuService, private spinner: NgxSpinnerService, private toastr: ToastsManager) {}

    ngOnInit(): void {
        this.getApps();
    }

    getApps() {
        this.spinner.show();
        this.service.getApps().then((apps) => {
            this.apps = apps.content;
            console.log(this.apps);
            this.spinner.hide();
        }).catch((err) => {
            this.toastr.error('Ocorreu um erro');
            this.spinner.hide();
        })
    }
}
