import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HHAuthService } from '../auth/';
import { User } from '../user/model/user.model';
import { Credencials } from '../user/model/credencials.model';

@Component({
    selector: 'hab-login',
    templateUrl: 'login.component.html',
    styleUrls: ['./login.css']
})
export class LoginComponent implements OnInit {
    authenticationError: boolean;
    showAuthError: boolean;
    credencials: Credencials;
    loading = false;
    success = false;
    private returnUrl: string;
    constructor(private authService: HHAuthService,
        private router: Router,
        private activeRoute: ActivatedRoute) {
        this.credencials = new Credencials();
    }
    ngOnInit() {
        this.credencials = new Credencials();
        this.activeRoute
            .queryParams
            .subscribe((params) => {
                this.returnUrl = params.returnUrl;
                this.success = params.success;
            });

    }
    onLogin() {
        this.loading = true;
        this.authService.login(this.credencials).then(this.loginSuccess).catch(this.loginError);

    }
    private loginSuccess = (user: User) => {
        if (this.returnUrl) {
            console.log('using return url ', this.returnUrl);
            window.location.href = this.returnUrl;
            return;
        }
        this.router.navigate(['/']);
        this.loading = false;
    };
    private loginError = (err) => {
        console.log('err: ', err);
        this.authenticationError = this.showAuthError = true;
        this.loading = false;
    };

    goToRegisto() {
        this.router.navigate(['/registo']);
    }

}
