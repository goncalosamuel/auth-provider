import { platformBrowser } from '@angular/platform-browser';
import { ProdConfig } from './blocks/config/prod.config';
import { AuthServerAppModule } from './app.module';

ProdConfig();

platformBrowser().bootstrapModule(AuthServerAppModule)
.then((success) => console.log(`Application started`))
.catch((err) => console.error(err));
