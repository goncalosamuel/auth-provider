import { Component, ViewContainerRef } from '@angular/core';
import { ToastModule, ToastOptions, ToastsManager } from 'ng2-toastr/ng2-toastr';

@Component({
  selector: 'hab-main',
  templateUrl: './app.component.html'
})
export class AppComponent {
  title = 'CGI Login Provider';
  constructor(public toast: ToastsManager, public toastOptions: ToastOptions, public vcr: ViewContainerRef) {
    this.toast.setRootViewContainerRef(vcr);
    this.toastOptions.showCloseButton = true;
    this.toastOptions.toastLife = 5000;
  }

}
