import { NgModule } from '@angular/core';
import { HHCommonModule } from '../common';
import { FormsModule } from '@angular/forms';
import { PasswordRecuperacaoComponent, PasswordStrengthBarComponent, PasswordService } from './';

@NgModule({
    imports: [
        HHCommonModule,
        FormsModule
    ],
    declarations: [
        PasswordRecuperacaoComponent,
        PasswordStrengthBarComponent
    ],
    providers: [
        PasswordService
    ]
})
export class HHPasswordModule { }
