export * from './password-recuperacao/password-recuperacao.component';
export * from './password-strength-bar/password-strength-bar.component';
export * from './password.service';
export * from './password.module';
export * from './password.route';
