import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class PasswordService {

    constructor(private http: HttpClient) { }

    saveChange(newPassword: string): Promise<any> {
        // tslint:disable-next-line:no-shadowed-variable
        return new Promise((resolve, reject) => {
            this.http.post('password/change', newPassword).subscribe((response) => {
                resolve(response);
            }, (err) => {
                reject(err);
            });
        });
    }
    resetPassword(keyAndPassword: any): Promise<any> {
        // tslint:disable-next-line:no-shadowed-variable
        return new Promise((resolve, reject) => {
            this.http.post<any>('password/reset-finish', keyAndPassword).subscribe((response) => {
                resolve(response);
            }, (err) => {
                reject(err);
            });
        });
    }
}
