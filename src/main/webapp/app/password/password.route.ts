import { Route } from '@angular/router';
import { PasswordRecuperacaoComponent } from './password-recuperacao/password-recuperacao.component';

export const PASS_ROUTE: Route[] = [{
    path: 'password/reset',
    component: PasswordRecuperacaoComponent
}]
