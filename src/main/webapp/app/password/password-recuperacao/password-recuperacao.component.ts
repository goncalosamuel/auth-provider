import { Component, OnInit, AfterViewInit, Renderer, ElementRef } from '@angular/core';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ActivatedRoute, Router } from '@angular/router';
import { PasswordService } from '../password.service';

@Component({
    selector: 'hab-password-recuperacao',
    templateUrl: './password-recuperacao.component.html',
    styleUrls: ['../password.css']
})
export class PasswordRecuperacaoComponent implements OnInit, AfterViewInit {
    confirmPassword: string;
    doNotMatch: string;
    error: string;
    keyMissing: boolean;
    resetAccount: any;
    success: string;
    modalRef: NgbModalRef;
    key: string;

    constructor(
        private passwordService: PasswordService,
        private route: ActivatedRoute,
        private router: Router,
        private elementRef: ElementRef, private renderer: Renderer
    ) {
    }

    ngOnInit() {
        this.route.queryParams.subscribe((params) => {
            this.key = params['key'];
        });
        this.resetAccount = {};
        this.keyMissing = !this.key;
    }

    ngAfterViewInit() {
        if (this.elementRef.nativeElement.querySelector('#password') != null) {
          this.renderer.invokeElementMethod(this.elementRef.nativeElement.querySelector('#password'), 'focus', []);
        }
    }

    finishReset() {
        this.doNotMatch = null;
        this.error = null;
        if (this.resetAccount.password !== this.confirmPassword) {
            this.doNotMatch = 'ERROR';
        } else {
            this.passwordService.resetPassword({key: this.key, newPassword: this.resetAccount.password}).then(() => {
                this.success = 'OK';
                this.router.navigate(['../../login'], { queryParams: { success: 'OK' } });
            }, () => {
                this.success = null;
                this.error = 'ERROR';
            });
        }
    }

}
