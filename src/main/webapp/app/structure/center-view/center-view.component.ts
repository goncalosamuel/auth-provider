import { Component, OnInit, ViewChild } from '@angular/core';
import { User } from '../../user/model/user.model';
import { NgbPopover, NgbPopoverConfig } from '@ng-bootstrap/ng-bootstrap';
import { HHAuthService } from '../../auth';
import { UserService } from '../../user';
import { Authority } from '../../user/model/authority.model';
import { ActivatedRoute } from '@angular/router';

@Component({
  templateUrl: './center-view.component.html',
})
export class CenterViewComponent implements OnInit {
  @ViewChild('p') p: NgbPopover;
  public user: User;
  constructor(config: NgbPopoverConfig, private authService: HHAuthService, private userService: UserService, private route: ActivatedRoute) {
    config.placement = 'bottom';
  }

  ngOnInit() {
    this.user = new User();
    this.route
      .queryParams
      .subscribe((params) => {
        console.log('params: ', params);
      });
    this.userService.getCurrent().then((user: User) => {
      this.user = user;
    });

  }

  // TODO
  logout() {
    return this.authService.logout();
  }

  public isAdmin() {
    if (this.user != null && this.user.grantedAuthorities != null) {
      for (let i = 0; i < this.user.grantedAuthorities.length; i++) {
        if (this.user.grantedAuthorities[i].authority === 'AuthAdmin') {
          return true;
        }
      }
      return false;
    }
  }

}
