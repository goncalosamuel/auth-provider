import { OAuthTokenResponse } from './../auth/oauth-response';
import { Injectable } from '@angular/core';
import { Credencials } from './model/credencials.model';
import { User } from './model/user.model';
import { resolve } from 'dns';
import { HttpClient, HttpHeaders, } from '@angular/common/http';
import { RegistoModel } from './model/registo.model';
import { PageRequestModel } from '../common/model/page-request.model';
import { PagedResponse } from '../common/model/page-response.model';
import { TokenHolder } from '../auth/token.holder';
import { Authority } from './model/authority.model';
import { GrantedAuthority } from './model/granted-authority.model';



@Injectable()
export class UserService {
    constructor(private http: HttpClient) { }

    getCurrent(): Promise<User> {
        // tslint:disable-next-line:no-shadowed-variable
        return new Promise((resolve, reject) => {
            this.http.get<User>('user/current').subscribe((user: User) => {
                resolve(user);
            }, (err) => {
                reject(err);
            });

        });
    }


    // *
    getUsers(page?: PageRequestModel): Promise<PagedResponse<User>> {
        if (!page) {
            page = PageRequestModel.buildForPage(0);
        }
        // tslint:disable-next-line:no-shadowed-variable
        return new Promise((resolve, reject) => {
            this.http.get<PagedResponse<User>>('user/', {
                params: page.getAsParams()
            }).subscribe((usersPage) => {
                resolve(usersPage);
            }, (err) => {
                reject(err);
            });
        });
    }

    getUserBy(login: string): Promise<User> {

        return new Promise((resolve, reject) => {
            this.http.get<User>('user/search/findByLogin?login=' + login).subscribe((user: User) => {
                resolve(user);
            }, (err) => {
                reject(err);
            });

        });

    }

    getUsersFiltered(login?: string, pagination?: PageRequestModel):
        Promise<PagedResponse<User>> {
        if (!pagination) {
            pagination = PageRequestModel.buildForPage(0);
        }
        // tslint:disable-next-line:no-shadowed-variable
        return new Promise((resolve, reject) => {
            this.http.get<PagedResponse<User>>('user/search/findByLoginContainingIgnoreCase?login=' + login, {
                params: pagination.getAsParams()
            }).subscribe((usersPage) => {
                resolve(usersPage);
            }, (err) => {
                reject(err);
            });
        })

    }

    getDetalhesLdap(login: string): Promise<Map<string, string>> {
        // tslint:disable-next-line:no-shadowed-variable
        return new Promise((resolve, reject) => {
            this.http.get<Map<string, string>>('user/' + login + '/details').subscribe((detalhes: Map<string, string>) => {
                resolve(detalhes);
            }, (err) => {
                reject(err);
            });
        });
    }


    getAuthorities(): Promise<any> {
        console.log("Inside getauthorities")
        return new Promise((resolve, reject) => {
            this.http.get<Authority[]>('authority/').subscribe((authorities: any) => {
                resolve(authorities.content);
            }, (err) => {
                reject(err);
            });

        });

    }

    saveAuthorities(userId: number, authorityIds: number[]): Promise<User> {
        return new Promise((resolve, reject) => {
            this.http.post('user/' + userId + '/authority', authorityIds).subscribe((User: any) => {
                resolve(User.content);
            }, (err) => {
                reject(err);
            });
        });
    }


    create(user: User): Promise<User> {
        return new Promise((resolve, reject) => {
            this.http.post('user/', user).subscribe((User: any) => {
                resolve(User.content);
            }, (err) => {
                reject(err);
            });
        });
    }

    getAuthoritiesById(userId: number): Promise<any> {
        console.log("Inside getGrantedAuthorities")
        return new Promise((resolve, reject) => {
            this.http.get<GrantedAuthority[]>('authority/search/findAuthoritiesByUserId?userId=' + userId).subscribe((authorities: any) => {
                resolve(authorities.content);
            }, (err) => {
                reject(err);
            });
        });
    }

}