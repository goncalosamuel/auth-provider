import { Authority } from "./authority.model";

export class GrantedAuthority {
    public authority: String;


    constructor(authority: String) {
        this.authority = authority;
    }
}