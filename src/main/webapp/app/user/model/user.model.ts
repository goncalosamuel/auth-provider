import { PerfilUtils } from './perfil.model';
import { Authority } from './authority.model';
import { GrantedAuthority } from './granted-authority.model';

export class User {
    public userId: number;
    public login: string;
    public firstName: string;
    public lastName: string;
    public email: string;
    public grantedAuthorities: Array<GrantedAuthority>;
    public authTypeId: string;
    public perfil: PerfilUtils;

    constructor(userId?: number, login?: string, firstName?: string, lastName?: string,
        email?: string, grantedAuthorities?: Array<GrantedAuthority>, authTypeId?: string) {
            this.userId = userId;
            this.login = login;
            this.email = email;
            this.grantedAuthorities = grantedAuthorities;
            this.firstName = firstName;
            this.lastName = lastName;
            this.authTypeId=authTypeId;
    }

}
