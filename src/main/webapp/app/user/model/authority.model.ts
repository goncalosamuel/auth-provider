export class Authority {
    public authorityId: number;
    public authority: string;
    public authorityName: string;

    constructor(authority?: string) {}

}
