import { DadosPessoaisComponent } from './dados-pessoais/dados-pessoais.component';
import { ListaUtilizadoresComponent } from './lista-utilizadores/lista-utilizadores.component';
import { NgModule } from '@angular/core';
import { UserService } from './user.service';
import { HHCommonModule } from '../common';
import { FormsModule } from '@angular/forms';
import { DetalhesLdapComponent } from './detalhes-ldap/detalhes-ldap.component';
import { DetalhesUserComponent } from './detalhes-user/detalhes-user.component';

@NgModule({
    imports: [
        HHCommonModule,
        FormsModule
    ],
    declarations: [
        ListaUtilizadoresComponent,
        DadosPessoaisComponent,
        DetalhesLdapComponent,
        DetalhesUserComponent
    ],
    providers: [
        UserService
    ]
})
export class HHUserModule { }
