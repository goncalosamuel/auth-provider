import { OnInit, Component } from '@angular/core';
import { UserService } from '../user.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ToastsManager } from 'ng2-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { User } from '../model/user.model';
import { PagedResponse } from '../../common/model/page-response.model';
import { PageRequestModel } from '../../common/model/page-request.model';
import { Authority } from '../model/authority.model';

@Component({
    selector: 'hab-detalhes-ldap',
    templateUrl: './detalhes-ldap.component.html',
    styles: []
})
export class DetalhesLdapComponent implements OnInit {

    public detalhesLdap = new Map<string, string>();
    public login: string;
    public keys: string[] = [];
    public values: string[] = [];

    public currentUser: User = new User();
    public authorities = '';
    public fullName = '';
    public email: string;
    public auth: Authority[];
    public grantedAuthorities: Authority[];
    public AuthoritysSave: number[];
    public contentEditable: boolean;

    public usersPage: PagedResponse<User>;
    public pagination: PageRequestModel = PageRequestModel.build();


    constructor(private service: UserService, private activatedRoute: ActivatedRoute, private toastr: ToastsManager,
    private router: Router, private spinner: NgxSpinnerService) {

    }

    ngOnInit(): void {
        this.AuthoritysSave = [];
        this.carregarDados();
        this.spinner.show();
        this.activatedRoute.params.subscribe((params: Params) => {
            this.login = params['login'];
        })
        this.service.getDetalhesLdap(this.login).then((detalhes: Map<string, string>) => {
            this.detalhesLdap = detalhes;
            console.log(this.detalhesLdap);
            // not possible to iterate a map with ngFor. might be possible with a custom Pipe
            // tslint:disable-next-line:forin
            for (const key in this.detalhesLdap) {
                this.keys.push(key);
                this.values.push(this.detalhesLdap[key]);
            }
            this.spinner.hide();
        }).catch((err) => {
            console.log(err);
            if (err.status === 500) {
                this.toastr.error('Ocorreu um erro interno');
            } else if (err.status === 404) {
                this.toastr.error('Este recurso não existe');
            } else if (err.status === 403 || err.status === 401) {
                this.toastr.error('Não tem permissões para aceder a este recurso');
            } else {
                this.toastr.error(err.error);
            }
            this.router.navigateByUrl('/');
            this.spinner.hide();

        });
    }



    carregarDados() {
        
        console.log("carregarDados")
        this.activatedRoute.params.subscribe((params: Params) => {
            this.login = params['login'];

        })

        this.service.getUserBy(this.login).then((user: User) => {
            this.currentUser = user;
            if(this.currentUser.firstName == undefined) this.currentUser.firstName="";
            if(this.currentUser.lastName == undefined) this.currentUser.lastName="";
            this.fullName = this.currentUser.firstName + ' ' + this.currentUser.lastName;
            this.email = user.email;
            this.getPersonalAuthorities(user.userId);
            
            if (user.grantedAuthorities != undefined) {
                user.grantedAuthorities.forEach((grantedAuthorities, index) => {
                    this.authorities += grantedAuthorities.authority;
                    if (index < user.grantedAuthorities.length - 1) {
                        this.authorities += ', ';
                    }                 
                })
            }
            this.getAllAuthorities();           
        })     
        console.log('USER AUTHTYPEID    '+this.currentUser.authTypeId);

        console.log('GRANTED AUTHORITIES    ->' + this.authorities);
    }


    getAllAuthorities() {
        this.service.getAuthorities().then((authorities: Authority[]) => {
            console.log("Inside getauthorities then")
            console.log("Authories received: ", authorities);
            this.auth = authorities;
        }, (error) => {
            console.log('erro');
        })
        console.log(this.auth);
        console.log("This currentUser:   " + this.currentUser.login);
    }

    //get of the users Authorities by id number
    getPersonalAuthorities(id: number) {
        this.service.getAuthoritiesById(id).then((authorities: Authority[]) => {
            this.grantedAuthorities = authorities;
        }, (error) => {
            console.log('erro');
        })
    }

    goBackPage() {
        this.router.navigateByUrl('/lista-utilizadores');
    }

    compareAuth(auth: Authority) {
        
         let result = false;
         this.grantedAuthorities.forEach(element => {
             if (element.authorityId === auth.authorityId) {
                 result = true;
             }
         });       
         return result;
    }


    toggleEditable(event,auth: Authority) {
        if( event.target.checked ) {
            this.AuthoritysSave.push(auth.authorityId);
    }
        else{
            for(let i = 0;i<this.grantedAuthorities.length;i++){
                if(this.grantedAuthorities[i].authorityId===auth.authorityId){
                    console.log("TOU CA DENTRO")
                    this.grantedAuthorities.splice(i,1);
                    console.log("AuthoritySave       ",this.grantedAuthorities);
                }
            }
            for(let i = 0;i<this.AuthoritysSave.length;i++){
                    if(this.AuthoritysSave[i]===auth.authorityId){
                        console.log("TOU CA DENTRO")
                        this.AuthoritysSave.splice(i,1);
                        console.log("AuthoritySave       ",this.AuthoritysSave);
            
                    }
            }
        }
    }


    removeDuplicates(arr){
        let unique_array = []
        for(let i = 0;i < arr.length; i++){
            if(unique_array.indexOf(arr[i]) == -1){
                unique_array.push(arr[i])
            }
        }
        return unique_array
    }
    getValue() {
        
        this.spinner.show();
        this.grantedAuthorities.forEach(element => {
            this.AuthoritysSave.push(element.authorityId);
        });
        
        //Service to Save Authorities
        this.service.saveAuthorities(this.currentUser.userId,this.AuthoritysSave).then((user: User) => {
             this.currentUser= user;        
            console.log("Inside getauthorities then:         " + this.grantedAuthorities)
        }, (error) => {
            console.log('erro');
        })


        //Service to Update User
        
        let nameNoSpace = this.fullName.split(" ");
        this.currentUser.firstName = nameNoSpace[0];
        if(nameNoSpace.length>1) this.currentUser.lastName = nameNoSpace[nameNoSpace.length-1];
        this.currentUser.grantedAuthorities= null;
        this.service.create(this.currentUser).then((user: User) => {
            this.spinner.hide();
            this.goBackPage();
            this.toastr.success('Sucesso ao editar detalhes');
            this.currentUser = user;

            
            
        }).catch((err) => {
            console.log(err);
            if (err.status === 500) {
                this.toastr.error('Ocorreu um erro interno');
            } else if (err.status === 404) {
                this.toastr.error('Este recurso não existe');
            } 
            this.spinner.hide();

        });

        
      
    }    

}
