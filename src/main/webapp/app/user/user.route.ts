import { Route } from '@angular/router';
import { PERFIL } from './model/perfil.model';
import { ListaUtilizadoresComponent } from './lista-utilizadores/lista-utilizadores.component';
import { DadosPessoaisComponent } from './dados-pessoais/dados-pessoais.component';
import { DetalhesLdapComponent } from './detalhes-ldap/detalhes-ldap.component';
import { DetalhesUserComponent } from './detalhes-user/detalhes-user.component';

export const LISTA_UTILIZADORES_ROUTE: Route = {
        path: 'lista-utilizadores',
        component: ListaUtilizadoresComponent,
}

export const DADOS_PESSOAIS_ROUTE: Route =  {
        path: 'dados-pessoais',
        component: DadosPessoaisComponent
}

export const DETALHES_LDAP_ROUTE: Route = {
        path: ':login/detalhesLdap',
        component: DetalhesLdapComponent
}

export const DETALHES_EDITAR_ROUTE: Route = {
        path: ':login/detalhesUser',
        component: DetalhesUserComponent
}
