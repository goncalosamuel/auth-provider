import { OnInit, Component } from '@angular/core';
import { UserService } from '../user.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ToastsManager } from 'ng2-toastr';
import { User } from '../model/user.model';
import { NgxSpinnerService } from 'ngx-spinner';
import { userInfo } from 'os';
import { PageRequestModel } from '../../common/model/page-request.model';
import { PagedResponse } from '../../common/model/page-response.model';
import { Authority } from '../model/authority.model';
import { GrantedAuthority } from '../model/granted-authority.model';


@Component({
    selector: 'hab-detalhes-user',
    templateUrl: './detalhes-user.component.html',
    styles: []
})
export class DetalhesUserComponent implements OnInit {
    public currentUser: User = new User();
    public authorities = '';
    public fullName = '';
    public login: string;
    public email: string;
    public auth: Authority[];
    public grantedAuthorities: Authority[];
    public AuthoritysSave: number[];
    public contentEditable: boolean;

    public usersPage: PagedResponse<User>;
    public pagination: PageRequestModel = PageRequestModel.build();

    constructor(private service: UserService, private router: Router,
        private toastr: ToastsManager, private activatedRoute: ActivatedRoute, private spinner: NgxSpinnerService) {

    }

    ngOnInit(): void {
        console.log('init');
        this.AuthoritysSave = [];
        this.carregarDados();
        
    }

    carregarDados() {
        
        console.log("carregarDados")
        this.activatedRoute.params.subscribe((params: Params) => {
            this.login = params['login'];

        })

        this.service.getUserBy(this.login).then((user: User) => {
            this.currentUser = user;
            if(this.currentUser.firstName == undefined) this.currentUser.firstName="";
            if(this.currentUser.lastName == undefined) this.currentUser.lastName="";
            this.fullName = this.currentUser.firstName + ' ' + this.currentUser.lastName;
            this.email = user.email;
            this.getPersonalAuthorities(user.userId);

            if (user.grantedAuthorities != undefined) {
                user.grantedAuthorities.forEach((grantedAuthorities, index) => {
                    this.authorities += grantedAuthorities.authority;
                    if (index < user.grantedAuthorities.length - 1) {
                        this.authorities += ', ';
                    }                 
                })
            }
            this.getAllAuthorities();           
        })     
        console.log('GRANTED AUTHORITIES    ->' + this.authorities);
    }


    getAllAuthorities() {
        this.service.getAuthorities().then((authorities: Authority[]) => {
            console.log("Inside getauthorities then")
            console.log("Authories received: ", authorities);
            this.auth = authorities;
        }, (error) => {
            console.log('erro');
        })
        console.log(this.auth);
        console.log("This currentUser:   " + this.currentUser.login);
    }

    //get of the users Authorities by id number
    getPersonalAuthorities(id: number) {
        this.service.getAuthoritiesById(id).then((authorities: Authority[]) => {
            this.grantedAuthorities = authorities;
        }, (error) => {
            console.log('erro');
        })
    }

    goBackPage() {
        this.router.navigateByUrl('/lista-utilizadores');
    }

    compareAuth(auth: Authority) {
        
         let result = false;
         this.grantedAuthorities.forEach(element => {
             if (element.authorityId === auth.authorityId) {
                 result = true;
             }
         });       
         return result;
    }


    toggleEditable(event,auth: Authority) {
        if( event.target.checked ) {
            this.AuthoritysSave.push(auth.authorityId);
    }
        else{
            for(let i = 0;i<this.grantedAuthorities.length;i++){
                if(this.grantedAuthorities[i].authorityId===auth.authorityId){
                    console.log("TOU CA DENTRO")
                    this.grantedAuthorities.splice(i,1);
                    console.log("AuthoritySave       ",this.grantedAuthorities);
                }
            }
            for(let i = 0;i<this.AuthoritysSave.length;i++){
                    if(this.AuthoritysSave[i]===auth.authorityId){
                        console.log("TOU CA DENTRO")
                        this.AuthoritysSave.splice(i,1);
                        console.log("AuthoritySave       ",this.AuthoritysSave);
            
                    }
            }
        }
    }


    removeDuplicates(arr){
        let unique_array = []
        for(let i = 0;i < arr.length; i++){
            if(unique_array.indexOf(arr[i]) == -1){
                unique_array.push(arr[i])
            }
        }
        return unique_array
    }
    getValue() {
        
        this.spinner.show();
        this.grantedAuthorities.forEach(element => {
            this.AuthoritysSave.push(element.authorityId);
        });
        
        //Service to Save Authorities
        this.service.saveAuthorities(this.currentUser.userId,this.AuthoritysSave).then((user: User) => {
             this.currentUser= user;        
            console.log("Inside getauthorities then:         " + this.grantedAuthorities)
        }, (error) => {
            console.log('erro');
        })


        //Service to Update User
        
        let nameNoSpace = this.fullName.split(" ");
        this.currentUser.firstName = nameNoSpace[0];
        if(nameNoSpace.length>1) this.currentUser.lastName = nameNoSpace[nameNoSpace.length-1];
        this.currentUser.grantedAuthorities= null;
        this.service.create(this.currentUser).then((user: User) => {
            this.spinner.hide();
           
            this.toastr.success('Sucesso ao editar detalhes ');
            this.currentUser = user;
            this.goBackPage();
            
        }).catch((err) => {
            console.log(err);
            if (err.status === 500) {
                this.toastr.error('Ocorreu um erro interno');
            } else if (err.status === 404) {
                this.toastr.error('Este recurso não existe');
            } 
            this.spinner.hide();

        });

        
      
    }    
}




