import { UserService } from './../user.service';
import { OnInit, Component } from '@angular/core';
import { ActivatedRouteSnapshot, ActivatedRoute, Router } from '@angular/router';
import { User } from '../model/user.model';
import { TokenHolder } from '../../auth';

@Component({
    selector: 'hab-dados-pessoais',
    templateUrl: './dados-pessoais.component.html',
    styles: []
})
export class DadosPessoaisComponent implements OnInit {

    // public currentUser: User;
    public currentUser: User = new User();
    public authorities = '';
    public fullName = '';

    constructor(private uService: UserService, private router: Router) {

    }

    ngOnInit(): void {
        console.log('init');
        this.carregarDados();
    }

    carregarDados() {
        this.uService.getCurrent().then((user: User) => {
            this.currentUser = user;
            this.fullName = user.firstName + ' ' + user.lastName;           
            console.log("This current grantedAuthorities: "+user.grantedAuthorities);       
            user.grantedAuthorities.forEach((grantedAuthorities, index) => {
                this.authorities += grantedAuthorities.authority;
                if (index < user.grantedAuthorities.length - 1) {
                    this.authorities += ', ';
                }
            })
        
        })
    }

    fechar() {
        this.router.navigateByUrl('/');
      }
}
