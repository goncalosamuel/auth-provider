import { Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { ActivatedRouteSnapshot, ActivatedRoute, Router } from '@angular/router';
import { ProcuraClienteModel } from './procura-cliente.model';
import { Observable } from 'rxjs/Observable';
import { UserService } from '..';
import { User } from '../model/user.model';
import { PageRequestModel } from '../../common/model/page-request.model';
import { PagedResponse } from '../../common/model/page-response.model';
import { Location } from '@angular/common';
import { Authority } from '../model/authority.model';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { NgxSpinnerService } from 'ngx-spinner';


@Component({
  selector: 'hab-lista-utilizadores',
  templateUrl: './lista-utilizadores.component.html',
  styles: []
})
export class ListaUtilizadoresComponent implements OnInit {

  public ultimaPesquisa = '';
  public login = '';
  public currentUser : User = new User();
  public usersPage: PagedResponse<User>;
  public pagination: PageRequestModel = PageRequestModel.build();
  public loading: boolean;
  constructor(private activeRoute: ActivatedRoute,
    private service: UserService,
    private location: Location,
    private router: Router,
    private toastr: ToastsManager,
    private spinner: NgxSpinnerService) {
  }

  ngOnInit() {
    this.procurar();
  }

  procurar(paginacao?: boolean) {
    this.spinner.show();
    if (paginacao) {
      this.login = this.ultimaPesquisa;
    } else {
      this.ultimaPesquisa = this.login;
    }
    this.service.getUsersFiltered(this.login, this.pagination)
      .then((usersPage) => {
        console.log('usersPage:12 ', usersPage);
        this.usersPage = usersPage;
        this.pagination.totalElements = usersPage.totalElements;
        this.spinner.hide();
      }).catch((err) => {
        if (err.status === 500) {
          this.toastr.error('Ocorreu um erro interno');
        } else if (err.status === 404) {
          this.toastr.error('Este recurso não existe');
        } else if (err.status === 403 || err.status === 401) {
          this.toastr.error('Não tem permissões para aceder a este recurso');
        } else {
          this.toastr.error(err.error);
        }
        this.spinner.hide();
      });
  }

  abrirDetalhes(user: User) {
      // this.service.getCurrent().then((user: User) => {
      //   this.currentUser = user;
      // })
      console.log
      if(user.authTypeId === 'LDAP') { //é LDAP, abre detalhes LDAP
        this.router.navigateByUrl('/' + user.login + '/detalhesLdap');
      }
      else  {
        this.router.navigateByUrl('/' + user.login + '/detalhesUser');
      }
  }
}
