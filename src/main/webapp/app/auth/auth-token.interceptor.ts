import { HttpInterceptor, HttpRequest, HttpEvent, HttpHandler, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Injector, Injectable, OnInit, ViewContainerRef } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { TokenHolder } from '.';
import { Router } from '@angular/router';
import { ToastsManager  } from 'ng2-toastr';

/**
 * This is a request interceptor
 * It's purpose is to add the authorization token to each request
 */
@Injectable()
export class AuthTokenInterceptor implements HttpInterceptor, OnInit {
    private token: string;

    constructor(
        private tokenHolder: TokenHolder,
        private router: Router,
        private toastr: ToastsManager
        // private vcr: ViewContainerRef

    ) {
        // this.toastr.setRootViewContainerRef(vcr);
        this.tokenHolder.getToken().subscribe((oAuthToken) => {
            console.log('oAuthToken: ', oAuthToken);
            this.token = oAuthToken.token_type + ' ' + oAuthToken.access_token;

        })
    }
    // Isto nao corre, vai-se la saber porque...
    ngOnInit() {
        /*  this.session.getSession().subscribe(session => {
             this.token = session.token;
         }); */
    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (!this.accept(req) || !this.token) {
            return next.handle(req);
        }

        const cloned = req.clone({
            headers: req.headers.set('authorization',
                this.token)
        });
        return next.handle(cloned).catch((err) => this.handleAuthError(err));

    }
    /** Only accepts if the url is different from login **/
    accept(req: HttpRequest<any>): boolean {
        console.log('accept req.headers.has(authorization):', req.headers.has('authorization'));
        return !req.headers.has('authorization');
    }
    private handleAuthError(err: HttpErrorResponse): Observable<any> {
        // handle your auth error or rethrow
        console.log('server error : ', err);
        if (err.status === 401) {
            // navigate /delete cookies or whatever
            this.tokenHolder.invalidate();
            // if you've caught / handled the error, you don't want to rethrow it unless you also want downstream consumers to have to handle it as well.
            // return Observable.of(err.message);
        } else if (err.status === 403) {
            // forbidden
            this.router.navigateByUrl('/');
            // this.toastr.warning('Acesso negado','Não tem acesso a funcionalidade!');

        }
        return Observable.throw(err);
    }
}
