package pt.cgi.ams.authserver.security.ldap;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.Objects;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;
import pt.cgi.ams.authserver.exception.ADException;

/**
 *
 * @author silvagc
 */
@Component
@Scope("singleton")
public class ADLegacyAuthenticator implements ADService {

    private final ADAuthenticationConfig config;
    //change the attributes to be returned. * returns everything
    private static final String[] ATTRIBUTES = {"*"}; //{"sn", "email" }
    private static final Logger LOG = LoggerFactory.getLogger(ADLegacyAuthenticator.class);

    public ADLegacyAuthenticator(ADAuthenticationConfig config) {
        this.config = config;
    }

    public ADAuthenticationConfig getConfig() {
        return config;
    }

    @Override
    public Map getDetails(String user) throws AuthenticationException {
        LOG.debug("using config: {}", config);
        String returnedAtts[] = ATTRIBUTES;
        String searchFilter = "(&(objectClass=user)(sAMAccountName=" + user + "))";

        //Create the search controls
        SearchControls searchCtls = new SearchControls();
        searchCtls.setReturningAttributes(returnedAtts);

        //Specify the search scope
        searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);

        Hashtable<String, String> env = new Hashtable<>();
        //

        LOG.debug("Created hashtable");
        env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");

        env.put("com.sun.jndi.ldap.connect.timeout", "" + config.getConnectTimeoutMs());
        env.put("com.sun.jndi.ldap.read.timeout", "" + config.getReadTimeOutMs());

        env.put(Context.PROVIDER_URL, config.getLdapHost());
        env.put(Context.SECURITY_AUTHENTICATION, "simple");
        env.put(Context.SECURITY_PRINCIPAL, config.getLdapUser() + "@" + config.getDomain());
        env.put(Context.SECURITY_CREDENTIALS, config.getLdapPassword());

        LOG.debug("After puts");
        LOG.info("USERNAME" + config.getLdapUser() + "@" + config.getDomain());
        LOG.info("PASSWORD" + config.getLdapPassword());
        Map<String, Object> amap = new HashMap();
        try {
            LdapContext ctxGC = new InitialLdapContext(env, null);
            //Search objects in GC using filters
            NamingEnumeration answer = ctxGC.search(config.getSearchBase(), searchFilter, searchCtls);
            while (answer.hasMoreElements()) {
                SearchResult sr = (SearchResult) answer.next();
                Attributes attrs = sr.getAttributes();
                if (attrs != null) {
                    NamingEnumeration ne = attrs.getAll();
                    while (ne.hasMore()) {
                        Attribute attr = (Attribute) ne.next();
                        amap.put(attr.getID(), attr.get());
                    }
                    ne.close();
                }
                break;
            }
        } catch (NamingException ne) {
            throw new ADException("NamingException: ", ne);
        }
        return amap;
    }

    @Override
    public boolean authenticate(String user, String pass) {
        LOG.debug("using config: {}", config);
        String returnedAtts[] = {"sn", "givenName", "mail"};
        String searchFilter = "(&(objectClass=user)(sAMAccountName=" + user + "))";

        //Create the search controls
        SearchControls searchCtls = new SearchControls();
        searchCtls.setReturningAttributes(returnedAtts);

        //Specify the search scope
        searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);

        Hashtable<String, String> env = new Hashtable<>();
        //
        env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");

        env.put("com.sun.jndi.ldap.connect.timeout", "" + config.getConnectTimeoutMs());
        env.put("com.sun.jndi.ldap.read.timeout", "" + config.getReadTimeOutMs());

        env.put(Context.PROVIDER_URL, config.getLdapHost());
        env.put(Context.SECURITY_AUTHENTICATION, "simple");
        env.put(Context.SECURITY_PRINCIPAL, user + "@" + config.getDomain());
        env.put(Context.SECURITY_CREDENTIALS, pass);

        LdapContext ctxGC = null;
        NamingEnumeration answer = null;
        try {
            ctxGC = new InitialLdapContext(env, null);
            //Search objects in GC using filters
            answer = ctxGC.search(config.getSearchBase(), searchFilter, searchCtls);

            while (answer.hasMoreElements()) {
                SearchResult sr = (SearchResult) answer.next();
                Attributes attrs = sr.getAttributes();
                Map amap = null;
                if (attrs != null) {
                    amap = new HashMap();
                    NamingEnumeration ne = attrs.getAll();
                    while (ne.hasMore()) {
                        Attribute attr = (Attribute) ne.next();
                        amap.put(attr.getID(), attr.get());
                    }
                    ne.close();
                }
                return true;
            }
        } catch (NamingException ne) {
            throw new ADException("NamingException: ", ne);
        }

        return false;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 13 * hash + Objects.hashCode(this.config);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ADLegacyAuthenticator other = (ADLegacyAuthenticator) obj;
        if (!Objects.equals(this.config, other.config)) {
            return false;
        }
        return true;
    }

}
