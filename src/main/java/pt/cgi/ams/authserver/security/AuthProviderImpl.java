package pt.cgi.ams.authserver.security;

import java.util.Collection;
import java.util.LinkedHashSet;
import pt.cgi.ams.authserver.security.ldap.ADLegacyAuthenticator;
import java.util.Set;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import pt.cgi.ams.authserver.db.User;
import static pt.cgi.ams.authserver.db.User.AuthType.LDAP;
import pt.cgi.ams.authserver.repository.UserRepository;

/**
 * If user has ldap auth_type then this class will check credencials against AD
 *
 * @author silvagc
 */
public class AuthProviderImpl
        implements AuthenticationProvider {

    private static final Logger LOG = LoggerFactory.getLogger(AuthProviderImpl.class);
    private final UserRepository userRepo;
    private final ADLegacyAuthenticator adAuthenticator;
    private final PasswordEncoder encoder;
    private final AuthentifiedUserStorage authStorage;

    public AuthProviderImpl(UserRepository userRepo, ADLegacyAuthenticator adAuthenticator, PasswordEncoder encoder, AuthentifiedUserStorage authStorage) {
        this.userRepo = userRepo;
        this.adAuthenticator = adAuthenticator;
        this.encoder = encoder;
        this.authStorage = authStorage;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        LOG.warn("authenticate {}", authentication);
        String login = authentication.getName();
        String password = authentication.getCredentials().toString();
        
        User user = userRepo.findByLogin(login).orElseThrow(() -> new UsernameNotFoundException("NOT FOUND"));

        LOG.debug("User {} is LDAP? {}", login, user.getAuthTypeId());
        if (user.getAuthTypeId().equals(LDAP)) {
            performADLogin(login, password);
        } else {
            // verify credencials ( rawPassword, encodedPassword [from database] )
            if (!encoder.matches(password, user.getPassword())) {
                throw new AuthenticationCredentialsNotFoundException("Unsucessful authentication using login " + login);
            }
        }
        Collection<org.springframework.security.core.GrantedAuthority> authorities = getSpringAuthorities(user);
        // add this user to AuthentifiedUserStorage
        authStorage.store(login, user);

        return new UsernamePasswordAuthenticationToken(
                new org.springframework.security.core.userdetails.User(user.getLogin(),
                        user.getPassword(), authorities), password, authorities);

    }

    private void performADLogin(String login, String password) throws AuthenticationException {
        adAuthenticator.authenticate(login, password);

    }

    @Override
    public boolean supports(Class<?> type) {
        return type.equals(
                UsernamePasswordAuthenticationToken.class);
    }

    public static Collection<org.springframework.security.core.GrantedAuthority> getSpringAuthorities(User user) {
        Set<org.springframework.security.core.GrantedAuthority> authorities = new LinkedHashSet<>();
        user.getGrantedAuthorities().forEach((ga) -> {
            authorities.add(new SimpleGrantedAuthority(ga.getAuthorityId().getAuthority()));
        });

        LOG.info("current authorities {} ", authorities);
        return authorities;
    }
}
