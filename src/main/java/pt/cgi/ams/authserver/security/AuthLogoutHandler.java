package pt.cgi.ams.authserver.security;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.stereotype.Component;

/**
 *
 * @author silvagc
 */
@Component
public class AuthLogoutHandler implements LogoutHandler {

    private static final Logger LOG = LoggerFactory.getLogger(AuthLogoutHandler.class);
    private final TokenStore tokenStore;
    private final UserService userService;

    public AuthLogoutHandler(TokenStore tokenStore,
            UserService userService) {
        this.tokenStore = tokenStore;
        this.userService = userService;
    }

    @Override
    public void logout(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
        String authUser = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String tokenValue = request.getHeader("Authorization").toLowerCase().replace("bearer", "").trim();
        LOG.debug("revoking token {} of user {}", tokenValue, authUser);
        OAuth2AccessToken accessToken = tokenStore.readAccessToken(tokenValue);
        if (accessToken.getRefreshToken() != null) {
            tokenStore.removeRefreshToken(accessToken.getRefreshToken());
        }
        tokenStore.removeAccessToken(accessToken);
        userService.logout(authUser);
    }
}
