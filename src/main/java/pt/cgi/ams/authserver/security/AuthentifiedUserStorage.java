package pt.cgi.ams.authserver.security;

import javax.annotation.PreDestroy;
import org.ehcache.Cache;
import org.ehcache.CacheManager;
import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.CacheManagerBuilder;
import org.ehcache.config.builders.ExpiryPolicyBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.ehcache.config.units.MemoryUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import pt.cgi.ams.authserver.db.User;

/**
 *
 * @author silvagc
 */
@Component
@Scope("singleton")
public class AuthentifiedUserStorage {

    private static final String CACHE_NAME = "AUTH_USER_CACHE";
    private static final Integer MAX_NUMBER_OF_ENTRIES = 5000;
    private static final Logger LOG = LoggerFactory.getLogger(AuthentifiedUserStorage.class);
    private static final CacheConfigurationBuilder<String, User> configurationBuilder
            = CacheConfigurationBuilder.newCacheConfigurationBuilder(String.class, User.class, ResourcePoolsBuilder
                    .heap(MAX_NUMBER_OF_ENTRIES)
                    .disk(10, MemoryUnit.MB, true))
                    .withExpiry(ExpiryPolicyBuilder.timeToLiveExpiration(SecurityConstants.ACCESS_TOKEN_TTL_DURATION));
    private static final CacheManager MANAGER = CacheManagerBuilder.newCacheManagerBuilder()
            .with(CacheManagerBuilder.persistence("AUTH_USER_CACHE"))
            .build(true);
    private static final Cache<String, User> REST_API_AUTH_USER_CACHE = MANAGER
            .createCache(CACHE_NAME, configurationBuilder);

    public void store(String login, User user) {
        REST_API_AUTH_USER_CACHE.put(login, user);
    }

    public boolean contains(String user) {
        return REST_API_AUTH_USER_CACHE.get(user) != null;
    }

    public User retrieve(String user) {
        return REST_API_AUTH_USER_CACHE.get(user);
    }
    public void remove(String login){
        REST_API_AUTH_USER_CACHE.remove(login);
    }

    @PreDestroy
    public void onDestroy() throws Exception {
        LOG.debug("Spring Container is destroyed!");
        MANAGER.close();
    }

}
