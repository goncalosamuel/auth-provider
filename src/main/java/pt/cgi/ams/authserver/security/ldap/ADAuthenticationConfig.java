package pt.cgi.ams.authserver.security.ldap;

import java.util.Objects;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.LdapContextSource;
import org.springframework.stereotype.Component;

/**
 *
 * @author silvagc
 */
@Component
public class ADAuthenticationConfig {

    @Value("${ldap.readTimeoutMs}")
    public int readTimeOutMs = 5000;
    @Value("${ldap.connectTimeoutMs}")
    public int connectTimeoutMs = 5000;
    @Value("${ldap.domain}")
    private String domain;
    @Value("${ldap.host}")
    private String ldapHost;
    @Value("${ldap.searchBase}")
    private String searchBase;
    @Value("${ldap.ldapUser}")
    private String ldapUser;
    @Value("${ldap.ldapPassword}")
    private String ldapPassword;

    @Bean
    public LdapContextSource contextSource() {
        LdapContextSource contextSource = new LdapContextSource();
        contextSource.setUrl(ldapHost);
        contextSource.setBase(searchBase);
        contextSource.setUserDn(ldapUser);
        contextSource.setPassword(ldapPassword);
        return contextSource;
    }

    @Bean
    public LdapTemplate ldapTemplate() {
        return new LdapTemplate(contextSource());
    }

    public String getLdapUser() {
        return ldapUser;
    }

    public void setLdapUser(String ldapUser) {
        this.ldapUser = ldapUser;
    }

    public int getReadTimeOutMs() {
        return readTimeOutMs;
    }

    public void setReadTimeOutMs(int readTimeOutMs) {
        this.readTimeOutMs = readTimeOutMs;
    }

    public int getConnectTimeoutMs() {
        return connectTimeoutMs;
    }

    public void setConnectTimeoutMs(int connectTimeoutMs) {
        this.connectTimeoutMs = connectTimeoutMs;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getLdapHost() {
        return ldapHost;
    }

    public void setLdapHost(String ldapHost) {
        this.ldapHost = ldapHost;
    }

    public String getSearchBase() {
        return searchBase;
    }

    public void setSearchBase(String searchBase) {
        this.searchBase = searchBase;
    }

    public String getLdapPassword() {
        return ldapPassword;
    }

    public void setLdapPassword(String ldapPassword) {
        this.ldapPassword = ldapPassword;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + Objects.hashCode(this.domain);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ADAuthenticationConfig other = (ADAuthenticationConfig) obj;
        if (!Objects.equals(this.domain, other.domain)) {
            return false;
        }
        if (!Objects.equals(this.ldapHost, other.ldapHost)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ADAuthenticationConfig{" + "domain=" + domain + ", ldapHost=" + ldapHost + ", searchBase=" + searchBase + '}';
    }

}
