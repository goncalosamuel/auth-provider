package pt.cgi.ams.authserver.security;

import java.util.HashMap;
import java.util.Map;
import javax.naming.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.LdapContextSource;
import org.springframework.ldap.filter.EqualsFilter;
import org.springframework.ldap.filter.Filter;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import pt.cgi.ams.authserver.db.User;
import pt.cgi.ams.authserver.exception.UserNotFoundException;
import pt.cgi.ams.authserver.repository.UserRepository;

/**
 *
 * This UserService class will try to login aggainst local database or LAP
 *
 * @author silvagc
 */
@Component
public class UserService {

    private static final Logger LOG = LoggerFactory.getLogger(UserService.class);
    private final UserRepository repo;
    private final AuthentifiedUserStorage storage;
    private final PasswordEncoder encoder;

    @Autowired
    public UserService(UserRepository repo,
            AuthentifiedUserStorage storage, PasswordEncoder encoder) {
        this.repo = repo;
        this.storage = storage;
        this.encoder = encoder;
    }

    public User getByLogin(String login) throws UserNotFoundException {
        User user = this.storage.retrieve(login);
        if (user == null) {
            LOG.info("cache miss, this sould happen very often");
            //no user found, we should get and store
            user = repo.findByLogin(login).orElseThrow(UserNotFoundException::new);
            storage.store(login, user);
        }
        return user;
    }

    public void logout(String login) {
        storage.remove(login);
    }

    public User completePasswordReset(String newPassword, String key) throws UserNotFoundException {
        LOG.debug("Reset user password for reset key {}", key);

        return repo.findOneByResetKey(key)
                .map(user -> {
                    String encryptedPassword = encoder.encode(newPassword);
                    user.setResetKey(null);
                    user.setResetDate(null);
                    user.setPassword(encryptedPassword);
                    return repo.save(user);
                }).orElse(null);
    }

    public static void main(String[] args) throws Exception {
        // Setup the LDAP client (normally done via Spring context file).
        Map<String, Object> baseEnvironmentProperties = new HashMap<String, Object>();
        baseEnvironmentProperties.put(Context.SECURITY_AUTHENTICATION, "GSSAPI");

        LdapContextSource contextSource = new LdapContextSource();
        contextSource.setUrl("LDAP://FI-DC013.groupinfra.com:389");
        contextSource.setBase("DC=groupinfra,DC=com");
        contextSource.setUserDn("app.IssueTracker.pt@groupinfra.com");
        contextSource.setPassword("password1234&&");
        contextSource.setBaseEnvironmentProperties(baseEnvironmentProperties);

        contextSource.afterPropertiesSet();

        LdapTemplate ldapTemplate = new LdapTemplate(contextSource);
        ldapTemplate.afterPropertiesSet();

        // Perform the authentication.
        Filter filter = new EqualsFilter("sAMAccountName", "silvagc");

        boolean authed = false;
        try {
            authed = ldapTemplate.authenticate("OU=Corporate",
                    filter.encode(),
                    "");
        } catch (org.springframework.ldap.AuthenticationException ae) {
            LOG.warn("auth exception: ", ae);
        }

        // Display the results.
        System.out.println("Authenticated: " + authed);
    }

}
