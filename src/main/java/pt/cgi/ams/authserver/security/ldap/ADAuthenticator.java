//package pt.cgi.ams.authserver.security.ldap;
//
//import java.util.Map;
//import javax.naming.NamingException;
//import javax.naming.directory.DirContext;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.ldap.core.ContextSource;
//import org.springframework.ldap.core.LdapTemplate;
//import org.springframework.stereotype.Component;
//
///**
// * uses spring ldap
// *
// * @author silvagc
// */
//@Component
//public class ADAuthenticator implements ADService {
//
//    private final LdapTemplate ldapTemplate;
//    private final ADAuthenticationConfig config;
//    @Autowired
//    public ADAuthenticator(LdapTemplate ldapTemplate,
//            ADAuthenticationConfig config) {
//        this.ldapTemplate = ldapTemplate;
//        this.config = config;
//    }
//
//    @Override
//    public Map getDetails(String user) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
//
//    @Override
//    public boolean authenticate(String username, String password) throws NamingException {
//        DirContext context = contextSource
//                .getContext(
//                        "cn="
//                        + username
//                        + ",ou=users,"
//                        + config.getSearchBase(), password);
//        
//        return ADUser.buildFrom(context);
//    }
//
//}
