package pt.cgi.ams.authserver.security.ldap;

import java.util.Map;
import javax.naming.directory.DirContext;

/**
 *
 * @author silvagc
 */
public class ADUser {

    

    private String firstName;
    private String surName;
    private String email;
    private String division;
    private String number;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
    
    

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDivision() {
        return division;
    }

    public void setDivision(String division) {
        this.division = division;
    }
    

    /**
     * This method should only be used with s valid AD Response
     *
     * @param adResponse
     */
    protected static ADUser buildFrom(Map<String, String> adResponse) {
        ADUser user = new ADUser();

        String firstName = adResponse.get(GIVEN_NAME_KEY);
        if (firstName != null) {
            firstName = firstName.split("\\s")[0];
        }
        user.setEmail(adResponse.get(EMAIL_KEY));
        user.setFirstName(firstName);
        user.setSurName(adResponse.get(SUR_NAME_KEY));
        user.setDivision(adResponse.get(DIVISION_KEY));
        user.setNumber(adResponse.get(EMPLOYEE_NUMBER_KEY));
        return user;
    }
    
   
    @Override
    public String toString() {
        return "ADUser{" + "firstName=" + firstName + ", surName=" + surName + ", email=" + email + '}';
    }
    

    private static final String EMAIL_KEY = "mail";
    private static final String GIVEN_NAME_KEY = "givenName";
    private static final String SUR_NAME_KEY = "sn";
    private static final String DIVISION_KEY = "division";
    //In CGI AD this is the email
    private static final String PRINCIPAL_NAME = "userPrincipalName";
    private static final String EMPLOYEE_NUMBER_KEY = "employeeID";
    
       
    /**
     * Centro custo ( division )
     * email
     * numero
     * username
     * data nascimento ( talvez )
     */
}
