package pt.cgi.ams.authserver.security;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;

@Configuration
@EnableResourceServer
@EnableWebSecurity(debug = false)
public class AuthResourceServer extends ResourceServerConfigurerAdapter {

    @Override
    public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
        resources.resourceId("webresource");
    }

//    @Override
//    public void configure(HttpSecurity http) throws Exception {
//        http.anonymous().and()
//                .authorizeRequests()
//                .antMatchers("oauth/token","/login", "/oauth/token/revokeById/**", "/tokens/**", "/password/reset-finish", "**.html", "**.js").permitAll()
//                .anyRequest().authenticated()
//                .and().formLogin().disable()
//                .csrf().disable();
//        // @formatter:on
//    }
}
