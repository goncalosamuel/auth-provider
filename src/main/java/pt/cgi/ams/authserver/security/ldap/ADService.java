package pt.cgi.ams.authserver.security.ldap;

import java.util.Map;
import javax.naming.NamingException;
import org.springframework.security.core.AuthenticationException;

/**
 *
 * @author silvagc
 */
public interface ADService {
    public Map getDetails(String user) throws AuthenticationException;
    public boolean authenticate(String user, String pass) throws AuthenticationException;
    
}
