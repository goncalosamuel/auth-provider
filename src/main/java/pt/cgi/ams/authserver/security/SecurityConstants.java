package pt.cgi.ams.authserver.security;

import java.time.Duration;

/**
 *
 * @author silvagc
 */
public class SecurityConstants {

    public static final Duration ACCESS_TOKEN_TTL_DURATION = Duration.ofHours(1);
    public static final int ACCESS_TOKEN_TTL_DURATION_SEC = Math.toIntExact(ACCESS_TOKEN_TTL_DURATION.getSeconds());
    public static final int REFRESH_TOKEN_TTL_DURATION_SEC = Math.toIntExact(Duration.ofDays(1).getSeconds());

    public static final String[] AUTH_GRANT_TYPES = {"password", "authorization_code", "client_credentials", "refresh_token"};
    public static final String[] AUTH_SCOPES = {"foo", "read", "write"};
}
