package pt.cgi.ams.authserver.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.firewall.HttpFirewall;
import org.springframework.security.web.firewall.StrictHttpFirewall;
import pt.cgi.ams.authserver.repository.UserRepository;
import pt.cgi.ams.authserver.security.ldap.ADLegacyAuthenticator;

/**
 * @author Gonçalo
 */
@Configuration
@EnableWebSecurity(debug = false)
public class ServerSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserRepository userRepo;
    @Autowired
    private ADLegacyAuthenticator adAuthenticator;
    @Autowired
    private AuthentifiedUserStorage authStorage;

    public ServerSecurityConfig() {
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth)
            throws Exception {

        auth.authenticationProvider(new AuthProviderImpl(userRepo, adAuthenticator, passwordEncoder(), authStorage));
    }

    /**
     * TODO
     *
     * @return
     */
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    @Bean
    public AuthenticationManager authenticationManagerBean()
            throws Exception {

        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(final HttpSecurity http) throws Exception {
        // @formatter:off
        http.anonymous().and()
                .authorizeRequests()
                .antMatchers("/user/**").authenticated()
                .and().formLogin().disable()
                .csrf().disable();
        // @formatter:on
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring()
                .antMatchers("/password/**")
                .antMatchers("/registo/**")
                .antMatchers("/lista-valores/**")
                .antMatchers("/traducao/**")
                .antMatchers("/socialLogin/**")
                .antMatchers("/*.{js,html}")
                .antMatchers("/")
                .antMatchers("/app/*.{js,html}")
                .antMatchers("/i18n/**")
                .antMatchers("/content/**");

        web.httpFirewall(allowUrlEncodedSlashHttpFirewall());

    }

    // decrypt password test
    public static void main(String[] args) {
        args = new String[]{"password"};
        if (args.length < 1) {
            throw new IllegalArgumentException("You must provide the plain text password for encryption");
        }
        String plainText = args[0];
        PasswordEncoder encoder = new BCryptPasswordEncoder();

    }

    @Bean
    public HttpFirewall allowUrlEncodedSlashHttpFirewall() {
        StrictHttpFirewall firewall = new StrictHttpFirewall();
        firewall.setAllowUrlEncodedSlash(true);
        firewall.setAllowSemicolon(true);
        return firewall;
    }

}
