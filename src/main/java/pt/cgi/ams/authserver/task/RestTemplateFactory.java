/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.cgi.ams.authserver.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.client.HttpComponentsAsyncClientHttpRequestFactory;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.token.grant.password.ResourceOwnerPasswordResourceDetails;
import org.springframework.stereotype.Component;

/**
 *
 * @author silvagc
 */
@Component
public class RestTemplateFactory {

    private final UserSyncConfig config;
    private final RestErrorHandler errorHandler;
    private static final Logger LOG = LoggerFactory.getLogger(RestTemplateFactory.class);

    @Autowired
    public RestTemplateFactory(UserSyncConfig config, RestErrorHandler errorHandler) {
        this.config = config;
        this.errorHandler = errorHandler;
    }

    @Bean("userSyncRestTemplate")
    private OAuth2RestTemplate restTemplate() {
        ResourceOwnerPasswordResourceDetails rDetails = new ResourceOwnerPasswordResourceDetails();

        rDetails.setGrantType(config.getGrandType());
        rDetails.setAccessTokenUri(config.getAuthUrl());
        rDetails.setClientId(config.getClientId());
        rDetails.setClientSecret(config.getClientSecret());

        rDetails.setUsername(config.getUsername());
        rDetails.setPassword(new String(config.getPassword()));
        LOG.debug("using the following ResourceOwnerPasswordResourceDetails:  ");
        LOG.debug("setGrantType: {} ", config.getGrandType());
        LOG.debug("getUrl: {} ", config.getAuthUrl());
        LOG.debug("getClientId: {} ", config.getClientId());
        LOG.debug("getClientSecret: {} ", config.getClientSecret());
        LOG.debug("getUsername: {} ", config.getUsername());
        //LOG.debug("getPassword: {} ", config.getPassword());
        OAuth2RestTemplate rTemplate = new OAuth2RestTemplate(rDetails);
        rTemplate.setErrorHandler(errorHandler);
        
        //rTemplate.setRequestFactory(new HttpComponentsAsyncClientHttpRequestFactory());
        return rTemplate;

    }

}
