package pt.cgi.ams.authserver.task;

import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.data.domain.Example;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;
import org.springframework.stereotype.Component;
import pt.cgi.ams.authserver.db.AuthProviderApplication;
import pt.cgi.ams.authserver.db.User;
import pt.cgi.ams.authserver.db.UserSynchronization;
import pt.cgi.ams.authserver.exception.InvalidUserSyncException;
import pt.cgi.ams.authserver.exception.RemoteAuthenticationException;
import pt.cgi.ams.authserver.exception.RemoteServiceException;
import pt.cgi.ams.authserver.exception.TooManySyncAttemptsException;
import pt.cgi.ams.authserver.exception.UserSyncException;
import pt.cgi.ams.authserver.repository.UserRepository;
import pt.cgi.ams.authserver.repository.UserSynchronizationRepo;
import pt.cgi.ams.authserver.resource.model.UserModel;

/**
 *
 * @author silvagc
 */
@Component
@Transactional
public class UserSyncTask {

    private final UserSynchronizationRepo userSyncRepo;
    private final UserRepository userRepo;
    private final OAuth2RestTemplate restTemplate;
    private static final Logger LOG = LoggerFactory.getLogger(UserSyncTask.class);
    /* The number of tries that a certain User synch will run before it stops */
    private static final int NUMBER_OF_TRIES = 5;

    public UserSyncTask(UserSynchronizationRepo userSyncRepo, OAuth2RestTemplate restTemplate,
            UserRepository userRepo) {
        this.userSyncRepo = userSyncRepo;
        this.restTemplate = restTemplate;
        this.userRepo = userRepo;
    }

    @Scheduled(cron = "${pt.cgi.ams.authserver.userSyncTask.cron.expression}")
    public void syncUsers() {
        LOG.info("*******************************************************************");
        LOG.info("*                                                                 *");
        LOG.info("*                  User Sync task started                         *");
        LOG.info("*                                                                 *");
        LOG.info("*******************************************************************");
        UserSynchronization syncExample = new UserSynchronization();
        syncExample.setCompleted(false);

        List<UserSynchronization> users = userSyncRepo.findAll(Example.of(syncExample));

        LOG.info("\tFound {} non complete user synchronizations", users.size());

        for (UserSynchronization sync : users) {

            try {
                syncUser(sync);
                onSuccessfulSync(sync);
            } catch (Exception e) {
                onSyncException(sync, e);
            }
        }

        LOG.info("*******************************************************************");
        LOG.info("*                                                                 *");
        LOG.info("*                  User Sync task finished                        *");
        LOG.info("*                                                                 *");
        LOG.info("*******************************************************************");
    }

    private void syncUser(UserSynchronization sync) throws UserSyncException, RemoteServiceException {
        User user = sync.getUser();
        AuthProviderApplication app = sync.getApplicationId();
        LOG.info("*\tsyncing user with id {} ", user.getUserId());
        LOG.info("*\tsyncing app with id {} ", app.getApplicationId());

        if (sync.getNumberOfTries() != null && sync.getNumberOfTries() > NUMBER_OF_TRIES) {
            LOG.warn("*\t\tThis sync has been retried to many times thus it will not run again");
            LOG.warn("*\t\tIf you need to trigger a retry please execute the sql script: ");
            LOG.warn("*\t\tUPDATE auth_provider_user_synch SET NUMBER_OF_TRIES = 0 WHERE USER_SYNC_ID = {} ", sync.getUserSynchronizationId());
            throw new TooManySyncAttemptsException();
        }

        if (user == null || app == null) {
            throw new IllegalArgumentException("user and app are mandatory. UserSynchronization id " + sync.getUserSynchronizationId());
        }
        URL url = null;
        try {
            url = new URL(app.getUserServiceUrl());
            LOG.info("*\t\tusing {} url for sync ", url);

        } catch (MalformedURLException mue) {
            throw new InvalidUserSyncException(mue);
        }

        sendUser(url, user.getUserId());
        
    }

    private void sendUser(URL url, Long userId) throws InvalidUserSyncException, RemoteAuthenticationException {
        User user = userRepo.getOne(userId);
        UserModel userModel = UserModel.build(user);
        LOG.debug("*\t\tPosting user {}", userModel);
        try {
            /**
             * sends the post request *
             */
            restTemplate.postForEntity(url.toURI(), userModel, Void.class);
        } catch (URISyntaxException mue) {
            throw new InvalidUserSyncException(mue);
        } catch (OAuth2Exception oauthException) {
            throw new RemoteAuthenticationException(oauthException);
        }

    }

    /**
     * Sets this sync as completed
     *
     * @param sync
     */
    private void onSuccessfulSync(UserSynchronization sync) {

        Integer numOfTries = sync.getNumberOfTries();
        sync.setCompleted(true);
        sync.setLastSyncDate(Calendar.getInstance().getTime());
        sync.setNumberOfTries(++numOfTries);
        userSyncRepo.save(sync);
        LOG.info("*\t\t Successful sync with id {} ", sync.getUserSynchronizationId());
    }

    private void onSyncException(UserSynchronization sync, Exception e) {
        LOG.warn("*\t\t sync [id={}] failed with exception message: ", sync.getApplicationId(), e);
        Integer numOfTries = Optional.ofNullable(sync.getNumberOfTries()).orElse(0);
        sync.setCompleted(false);
        sync.setLastSyncDate(Calendar.getInstance().getTime());
        sync.setNumberOfTries(++numOfTries);
        sync.setFailMessage(e.getMessage());
        userSyncRepo.save(sync);
    }
}
