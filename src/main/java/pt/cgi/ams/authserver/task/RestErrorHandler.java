package pt.cgi.ams.authserver.task;

import java.io.IOException;
import java.rmi.RemoteException;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.client.ResponseErrorHandler;
import pt.cgi.ams.authserver.exception.RemoteServiceException;


/**
 *
 * @author silvagc
 */
@Component
public class RestErrorHandler implements ResponseErrorHandler {

    private static final Logger LOG = LoggerFactory.getLogger(RestErrorHandler.class);

    @Override
    public boolean hasError(ClientHttpResponse response) throws IOException {
        HttpStatus status = response.getStatusCode();
        return status.is4xxClientError() || status.is5xxServerError();
    }

    @Override
    public void handleError(ClientHttpResponse response) throws IOException {
        LOG.warn("Error in request. response {}", response.getRawStatusCode());
        HttpStatus status = response.getStatusCode();

        if (status.value() == 401) {
            throw new RemoteServiceException("Unauthorized: token expired or invalid");
        }
        if (status.value() == HttpStatus.NOT_FOUND.value()) {
            throw new RemoteServiceException("");
        }
        if (!StringUtils.isEmpty(response.getStatusText())) {
            throw new RemoteServiceException(response.getStatusText());
        } else {
            throw new RemoteServiceException("status: " + response.getRawStatusCode());
        }

    }

}
