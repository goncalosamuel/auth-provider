package pt.cgi.ams.authserver.task;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 *
 * @author silvagc
 */
@Component
public class UserSyncConfig {

    /* oauth details */
    @Value("${pt.cgi.ams.authserver.userSyncTask.grandType:password}")
    private String grandType;
    @Value("${pt.cgi.ams.authserver.userSyncTask.clientId:LOGIN_APP}")
    private String clientId;
    // raw - not encoded
    @Value("${pt.cgi.ams.authserver.userSyncTask.clientSecret:secret}")
    private String clientSecret;
    @Value("${pt.cgi.ams.authserver.internalUser.username}")
    private String username;
    @Value("${pt.cgi.ams.authserver.internalUser.password}")
    private char[] password;
    @Value("${pt.cgi.ams.authserver.userSyncTask.authUrl}")
    private String authUrl;

    public String getGrandType() {
        return grandType;
    }

    public void setGrandType(String grandType) {
        this.grandType = grandType;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getClientSecret() {
        return clientSecret;
    }

    public void setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public char[] getPassword() {
        return password;
    }

    public void setPassword(char[] password) {
        this.password = password;
    }

    public String getAuthUrl() {
        return authUrl;
    }

    public void setAuthUrl(String authUrl) {
        this.authUrl = authUrl;
    }

    
}
