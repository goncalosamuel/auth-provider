package pt.cgi.ams.authserver;

import javax.persistence.PersistenceException;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurerAdapter;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import pt.cgi.ams.authserver.db.Authority;
import pt.cgi.ams.authserver.db.GrantedAuthority;
import pt.cgi.ams.authserver.db.User;
import pt.cgi.ams.authserver.repository.AuthorityRepositoryAndResource;
import pt.cgi.ams.authserver.repository.GrantedAuthorityRepositoryAndResource;
import pt.cgi.ams.authserver.repository.UserRepository;

@SpringBootApplication()
@EnableJpaAuditing
@EnableScheduling
public class AuthServerApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(AuthServerApplication.class, args);
    }

    @Bean
    public RepositoryRestConfigurerAdapter test() {

        return new RepositoryRestConfigurerAdapter() {
            @Override
            public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
                config.setDefaultMediaType(MediaType.APPLICATION_JSON);

            }

        };
    }

    /**
     * We need to make sure that the "internal user" exists If it doesn't exist
     * it must be created.
     */
    @Component
    public static class CommandLineAppStartupRunner implements CommandLineRunner {

        private final UserRepository userRepo;
        private final GrantedAuthorityRepositoryAndResource gaRepo;
        private final AuthorityRepositoryAndResource authorityRepo;
        private final PasswordEncoder encoder;
        @Value("${pt.cgi.ams.authserver.internalUser.username}")
        private String username;
        @Value("${pt.cgi.ams.authserver.internalUser.password}")
        private char[] password;

        @Autowired
        public CommandLineAppStartupRunner(UserRepository userRepo, GrantedAuthorityRepositoryAndResource gaRepo,
                AuthorityRepositoryAndResource authorityRepo,
                PasswordEncoder encoder) {
            this.userRepo = userRepo;
            this.gaRepo = gaRepo;
            this.authorityRepo = authorityRepo;
            this.encoder = encoder;
        }

        private static final Logger LOG
                = LoggerFactory.getLogger(CommandLineAppStartupRunner.class);

        public static int counter;

        /**
         * Warning: Any (uncaught) exception will result in the application
         * startup failure
         *
         * @param args
         * @throws Exception
         */
        @Override
        public void run(String... args) throws Exception {
            LOG.info("CommandLineAppStartupRunner started.");
            findOrCreateInternalUser();
            LOG.info("CommandLineAppStartupRunner ended");

        }

        /**
         * Tries to find the internal user using username given in the
         * properties file If the user is not found, then a new user is created
         */
        @Transactional
        private void findOrCreateInternalUser() {
            LOG.trace("findOrCreateInternalUser() called");
            User user = userRepo.findByLogin(username).orElseGet(() -> {
                LOG.info("No internal user found, i will create");
                LOG.debug("using username {} - password omited", username);
                User newUser = new User();
                newUser.setAuthTypeId(User.AuthType.LOCAL);
                newUser.setLogin(username);
                newUser.setPassword(encoder.encode(new String(password)));
                newUser.setFirstName("internal");
                newUser.setLastName("user");
                newUser.setEmail(username + "@cgi.com");

                try {
                    userRepo.save(newUser);
                    /* It must have the AuthAdmin credencials */
                    Authority authority = authorityRepo.findOneByAuthorityNameIgnoreCase("AuthAdmin");
                    if (authority == null) {
                        LOG.info("There's no AuthAdmin authority. I will try to create it");
                        authority = createAuthAdminAuthority();
                        LOG.info("AuthAdmin authority created");
                    }
                    LOG.info("AuthAdmin authority is about to be granted to the internal user");
                    GrantedAuthority ga = new GrantedAuthority();
                    ga.setAuthorityId(authority);
                    ga.setUserId(newUser);
                    gaRepo.save(ga);
                    LOG.info("AuthAdmin authority granted to {} using authorityId {}", newUser.getLogin(), ga.getAuthorityId());
                } catch (PersistenceException | javax.validation.ValidationException pEx) {
                    /* Any (uncaught) exception will result in the application startup failure */
                    LOG.warn("Exception creating internal user. Please make sure that this user is created! Exception will follow", pEx);
                }
                return newUser;
            });
        }

        private Authority createAuthAdminAuthority() {
            Authority auth = new Authority();
            auth.setAuthorityName("AuthAdmin");
            auth.setLevel(5);
            return authorityRepo.save(auth);
        }
    }
}
