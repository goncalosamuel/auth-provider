/*
 *
 */
package pt.cgi.ams.authserver;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.ClassPathResource;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;
import org.springframework.security.oauth2.provider.token.store.KeyStoreKeyFactory;

import static pt.cgi.ams.authserver.security.SecurityConstants.*;

/**
 * @author Gonçalo
 */
@Configuration
@EnableAuthorizationServer
@EnableGlobalMethodSecurity(prePostEnabled = true, proxyTargetClass = true)
public class AuthServerOAuth2Config
        extends AuthorizationServerConfigurerAdapter {

    private static final String LOGIN_APP_CLIENT = "LOGIN_APP";

    private final AuthenticationManager authenticationManager;
    private final PasswordEncoder encoder;
    private final ClientDetailsService userDetailsService;

    @Autowired
    public AuthServerOAuth2Config(AuthenticationManager authManager,
                                  PasswordEncoder encoder,
                                  ClientDetailsService userDetailsService) {
        this.authenticationManager = authManager;
        this.encoder = encoder;
        this.userDetailsService = userDetailsService;
    }


    @Override
    public void configure(final AuthorizationServerSecurityConfigurer oauthServer) throws Exception {
        oauthServer.tokenKeyAccess("permitAll()")
                .checkTokenAccess("permitAll()");
        //TODO IN PRODUCTION USE: .checkTokenAccess("isAuthenticated()");
    }


    @Override
    public void configure(final ClientDetailsServiceConfigurer clients) throws Exception {
        String secretEncoded = encoder.encode("secret");

        clients.inMemory()
                .withClient(LOGIN_APP_CLIENT)
                .secret(secretEncoded)
                .autoApprove(true)
                .authorizedGrantTypes(AUTH_GRANT_TYPES)
                .scopes(AUTH_SCOPES)
                .accessTokenValiditySeconds(ACCESS_TOKEN_TTL_DURATION_SEC)
                // 1 hour
                .refreshTokenValiditySeconds(REFRESH_TOKEN_TTL_DURATION_SEC)
                // 30 days
                .autoApprove(true);

    }

    @Bean
    @Primary
    public DefaultTokenServices tokenServices() {
        final DefaultTokenServices defaultTokenServices = new DefaultTokenServices();
        defaultTokenServices.setTokenStore(tokenStore());
        defaultTokenServices.setSupportRefreshToken(true);
        return defaultTokenServices;
    }

    @Override
    public void configure(final AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
        endpoints.tokenStore(tokenStore()).tokenEnhancer(accessTokenConverter())
                .authenticationManager(authenticationManager)
                .setClientDetailsService(userDetailsService);
    }

    @Bean
    public TokenStore tokenStore() {
        return new JwtTokenStore(accessTokenConverter());
    }

    @Bean
    public JwtAccessTokenConverter accessTokenConverter() {
        final JwtAccessTokenConverter converter = new CustomTokenEnhancer();
        final KeyStoreKeyFactory keyStoreKeyFactory = new KeyStoreKeyFactory(new ClassPathResource("mytest.jks"), "mypass".toCharArray());
        converter.setKeyPair(keyStoreKeyFactory.getKeyPair("mytest"));
        return converter;
    }

    @Bean
    public TokenEnhancer tokenEnhancer() {
        return new CustomTokenEnhancer();
    }

}
