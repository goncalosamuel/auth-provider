package pt.cgi.ams.authserver.resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import pt.cgi.ams.authserver.db.User;
import pt.cgi.ams.authserver.exception.InvalidPasswordException;
import pt.cgi.ams.authserver.exception.UserNotFoundException;
import pt.cgi.ams.authserver.resource.model.KeyAndPasswordModel;
import pt.cgi.ams.authserver.resource.model.ManagedUserModel;
import pt.cgi.ams.authserver.security.UserService;

/**
 * This controller goal is just to expose the "current" user in a sessionless
 * context. The auth token is used to map the desired user
 *
 * @author nuno.franco
 */
@RestController
public class PasswordController {

    private static final Logger LOG = LoggerFactory.getLogger(PasswordController.class);
    private final UserService userService;

    @Autowired
    public PasswordController(UserService userService) {
        this.userService = userService;
    }

    /**
     * POST   password/reset-finish : Finish to reset the password of the user
     *
     * @param keyAndPassword the generated key and the new password
     * @throws InvalidPasswordException 400 (Bad Request) if the password is incorrect
     */
    @PostMapping(path = "password/reset-finish")
    public void finishPasswordReset(@RequestBody KeyAndPasswordModel keyAndPassword) throws InvalidPasswordException, UserNotFoundException {

        if (!checkPasswordLength(keyAndPassword.getNewPassword())) {
            throw new InvalidPasswordException();
        }
        User user = userService.completePasswordReset(keyAndPassword.getNewPassword(), keyAndPassword.getKey());

        if (user == null) {
            throw new UserNotFoundException("No user was found for this reset key");
        }
    }

    private static boolean checkPasswordLength(String password) {
        return !StringUtils.isEmpty(password) &&
                password.length() >= ManagedUserModel.PASSWORD_MIN_LENGTH &&
                password.length() <= ManagedUserModel.PASSWORD_MAX_LENGTH;
    }

}
