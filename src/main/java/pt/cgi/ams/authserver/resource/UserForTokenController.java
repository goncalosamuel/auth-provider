package pt.cgi.ams.authserver.resource;

import java.util.Collection;
import java.util.Map;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import pt.cgi.ams.authserver.db.User;
import pt.cgi.ams.authserver.exception.ADException;
import pt.cgi.ams.authserver.exception.UserNotFoundException;
import pt.cgi.ams.authserver.resource.model.UserModel;
import pt.cgi.ams.authserver.security.UserService;
import pt.cgi.ams.authserver.security.ldap.ADLegacyAuthenticator;

/**
 * This controller goal is just to expose the "current" user in a sessionless
 * context. The auth token is used to map the desired user
 *
 * @author silvagc
 */
@Controller
public class UserForTokenController {

    private static final Logger LOG = LoggerFactory.getLogger(UserForTokenController.class);
    private final UserService userService;
    private final ADLegacyAuthenticator adAuth;
    private final TokenStore tokenStore;

    @Autowired
    public UserForTokenController(UserService userService, TokenStore tokenStore, ADLegacyAuthenticator adAuth) {
        this.userService = userService;
        this.tokenStore = tokenStore;
        this.adAuth = adAuth;
    }

//    @GetMapping("user/current")
//    public ResponseEntity<UserModel> getForToken() throws UserNotFoundException {
//        String authUser = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//        String login = authUser;
//        if (login == null) {
//            throw new AuthenticationCredentialsNotFoundException("no session");
//        }
//        User user = userService.getByLogin(login);
//        return ResponseEntity.ok(UserModel.buildWithAuthorities(user));
//    }
    @GetMapping("user/current")
    public ResponseEntity<UserModel> getForToken() throws UserNotFoundException {
        String authUser = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String login = authUser;
        if (login == null) {
            throw new AuthenticationCredentialsNotFoundException("no session");
        }
        User user = userService.getByLogin(login);
        return ResponseEntity.ok(UserModel.buildWithAuthorities(user));
    }

    @GetMapping("user/{user}/details")
    public ResponseEntity<Map> getDetails(@PathVariable String user) throws UserNotFoundException, NamingException {
        Map mapDetails = adAuth.getDetails(user);
        return ResponseEntity.ok(mapDetails);
    }

    @Order(1)
    @GetMapping("user/logout")
    public ResponseEntity<Collection<SimpleGrantedAuthority>> logout(@NotNull @RequestHeader("Authorization") String authHeader,
            OAuth2Authentication auth, HttpServletRequest request, HttpServletResponse response) {

        String authUser = (String) auth.getPrincipal();
        String tokenValue = authHeader.replace("bearer", "").trim();
        LOG.debug("revoking token {} of user {}", tokenValue, authUser);
        OAuth2AccessToken accessToken = tokenStore.readAccessToken(tokenValue);
        if (accessToken.getRefreshToken() != null) {
            tokenStore.removeRefreshToken(accessToken.getRefreshToken());
        }
        tokenStore.removeAccessToken(accessToken);
        userService.logout(authUser);

        SecurityContextLogoutHandler ctxLogOut = new SecurityContextLogoutHandler();
        ctxLogOut.logout(request, response, auth);

        return ResponseEntity.ok().build();
    }

}
