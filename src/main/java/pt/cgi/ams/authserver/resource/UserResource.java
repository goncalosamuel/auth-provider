package pt.cgi.ams.authserver.resource;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.validation.constraints.Size;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import pt.cgi.ams.authserver.db.AuthProviderApplication;
import pt.cgi.ams.authserver.db.Authority;
import pt.cgi.ams.authserver.db.GrantedAuthority;
import pt.cgi.ams.authserver.db.User;
import pt.cgi.ams.authserver.db.UserSynchronization;
import pt.cgi.ams.authserver.exception.AuthServerException;
import pt.cgi.ams.authserver.exception.ForbiddenException;
import pt.cgi.ams.authserver.exception.LoginAlreadyExistsException;
import pt.cgi.ams.authserver.exception.NotFoundException;
import pt.cgi.ams.authserver.exception.UserCreationException;
import pt.cgi.ams.authserver.repository.ApplicationRepositoryAndResource;
import pt.cgi.ams.authserver.repository.AuthorityRepositoryAndResource;
import pt.cgi.ams.authserver.repository.GrantedAuthorityRepositoryAndResource;
import pt.cgi.ams.authserver.repository.UserSynchronizationRepo;
import pt.cgi.ams.authserver.resource.model.UserModel;
import pt.cgi.ams.authserver.repository.UserRepository;
import pt.cgi.ams.authserver.security.AuthentifiedUserStorage;

/**
 *
 * @author silvagc
 */
@RestController
@RequestMapping("/user")
public class UserResource {

    private final UserRepository repository;
    private final UserSynchronizationRepo userSyncRepo;
    private final ApplicationRepositoryAndResource appsRepo;
    private final AuthorityRepositoryAndResource authorityRepo;
    private final GrantedAuthorityRepositoryAndResource grantedRepo;
    private final AuthentifiedUserStorage storage;
    private static final Logger LOG = LoggerFactory.getLogger(UserResource.class);

    public UserResource(
            UserRepository repository,
            UserSynchronizationRepo userSyncRepo,
            ApplicationRepositoryAndResource appsRepo,
            AuthorityRepositoryAndResource authorityRepo,
            GrantedAuthorityRepositoryAndResource grantedRepo, AuthentifiedUserStorage storage) {
        this.repository = repository;
        this.userSyncRepo = userSyncRepo;
        this.appsRepo = appsRepo;
        this.authorityRepo = authorityRepo;
        this.grantedRepo = grantedRepo;
        this.storage = storage;
    }

    @GetMapping("/")
    @PreAuthorize("hasAuthority('AuthAdmin')")
    public Collection<UserModel> list() {
        return repository.findAll().stream().
                map(user -> UserModel.buildWithAuthorities(user)).collect(Collectors.toList());
    }

    @GetMapping("/{userId}")
    @PreAuthorize("hasAuthority('AuthAdmin')")
    public UserModel get(@PathVariable("userId") Long userId) throws NotFoundException {
        LOG.debug("get called with {}", userId);
        return UserModel.buildWithSpringAuthorities(repository.findById(userId).orElseThrow(() -> new NotFoundException()));
    }

    /**
     * Set's the user authorities TODO ALLOW OTHER ROLES than AUthAdmin TO GRANT
     * AUTHORITIES
     *
     * @return
     */
    @Transactional()
    @PostMapping("/{userId}/authority/")
    //@PreAuthorize("hasAuthority('AuthAdmin')") now this is controlled by code
    public UserModel saveAuthorities(@PathVariable("userId") Long userId, @Size(min = 1) @RequestBody List<Long> authorityIds) throws NotFoundException, ForbiddenException {
        LOG.debug("saveAuthorities called with user_id {} and authorityIds {}", userId, authorityIds);

        User user = repository.findById(userId).orElseThrow(() -> new NotFoundException());

        List<GrantedAuthority> newUserAuthorities = new LinkedList<>();

        Collection<Authority> authorities = StreamSupport.stream(authorityRepo.findAllById(authorityIds).spliterator(), false)
                .collect(Collectors.toList());

        /**
         * removes all the current authorities
         */
        for (GrantedAuthority ga : user.getGrantedAuthorities()) {
            grantedRepo.delete(ga);
        }

        /**
         * now we create the new granted authorities
         */
        for (Authority authority : authorities) {
            GrantedAuthority gAuth = new GrantedAuthority();
            gAuth.setAuthorityId(authority);
            gAuth.setUserId(user);
            newUserAuthorities.add(gAuth);
        }
        user.setGrantedAuthorities(newUserAuthorities);
        /* let's make sure that our current user has the permission to create an user with this level of authorities */
        if (!hasPermissionToGrantAuthorities(user)) {
            throw new ForbiddenException("Current user doesn't have the right authorities to create");
        }
        /* saves the new grantedAuthorities */
        grantedRepo.saveAll(newUserAuthorities);
        /* now we must remove the old ones if any */

        return UserModel.buildWithSpringAuthorities(user);

    }

    /**
     *
     * When a user is updated it must but deleted from the auth cache so it can
     * get the update in the client
     *
     * @param userModel
     * @return
     * @throws UserCreationException
     * @throws ForbiddenException
     */
    public User update(UserModel userModel) throws AuthServerException, ForbiddenException {
        LOG.debug("Updating user, with model {} ", userModel);
        User toBeUpdated = repository.findById(userModel.getUserId()).orElseThrow(() -> new NotFoundException());
        toBeUpdated.setActivated(userModel.isActivated());
        toBeUpdated.setAuthTypeId(userModel.getAuthTypeId());
        toBeUpdated.setFirstName(userModel.getFirstName());
        toBeUpdated.setImageUrl(userModel.getImageUrl());
        toBeUpdated.setLangKey(userModel.getLangKey());
        toBeUpdated.setLastName(userModel.getLastName());
        /* login cannot be changed */
        //current.setLogin(login);
        /**
         * manager *
         */
        User manager = null;
        if (userModel.getManagerId() != null) {
            LOG.debug("Searching for manager with userId: {} ", userModel.getManagerId());
            /* If the userModel.managerId is null than the current will also be set to null */
            manager = repository.findById(userModel.getManagerId()).orElseThrow(() -> new NotFoundException("Manager not found"));
            LOG.debug("found manager. User is {} ", manager);
        }
        toBeUpdated.setManager(manager);
        storage.remove(toBeUpdated.getLogin());
        return this.repository.save(toBeUpdated);

    }

    @PreAuthorize("hasAuthority('AuthAdmin')")
    @RequestMapping(value = {"/"}, method = {RequestMethod.POST, RequestMethod.PUT})
    public UserModel create(@Valid @RequestBody UserModel userModel) throws ForbiddenException, AuthServerException {
        LOG.info("/user -> create() called");
        LOG.debug("with userModel {}", userModel);

        User createdUser = userModel.parse();

        /* if the user already exists it's an update */
        if (repository.findById(userModel.getUserId()).isPresent()) {
            LOG.debug("the user already exists, this is an update");
            createdUser = update(userModel);
        } else {
            LOG.warn("userModel.getUserId() {}", userModel.getUserId());
            LOG.warn("userModel.getUserId()  is present{}", this.repository.findByLogin(userModel.getLogin()).isPresent());

            if (this.repository.findByLogin(userModel.getLogin()).isPresent()) {
                throw new LoginAlreadyExistsException();
            };
            /* let's make sure that our current user has the permission to create an user with this level of authorities */
            if (!hasPermissionToGrantAuthorities(createdUser)) {
                throw new ForbiddenException();
            }
            User manager = null;
            if (userModel.getManagerId() != null) {
                /* If the userModel.managerId is null than the current will also be set to null */
                manager = repository.findById(userModel.getManagerId()).orElseThrow(() -> new NotFoundException("Manager not found"));
            }
            createdUser.setManager(manager);
            createdUser = this.repository.save(createdUser);
        }

        /* the user created or updated, let's flag the user as syncable to all the apps */
        int numberOfApps = 0;
        for (AuthProviderApplication app : getActivePushableApps()) {
            UserSynchronization userSync = new UserSynchronization();
            userSync.setApplicationId(app);
            userSync.setCompleted(false);
            userSync.setUser(createdUser);
            this.userSyncRepo.save(userSync);
            numberOfApps++;
        }
        LOG.debug("found {} active apps with pushUsers flag set to true", numberOfApps);

        return UserModel.build(createdUser);

    }

    @GetMapping("/search/findByLoginContainingIgnoreCase")
    @PreAuthorize("hasAuthority('AuthAdmin')")
    public Page<User> findByLogin(@Param("login") String login, Pageable pageable) {
        return repository.findByLoginContainingIgnoreCase(login, pageable);
    }

    @GetMapping("/search/findByLogin")
    @PreAuthorize("hasAuthority('AuthAdmin')")
    public User findByLogin(@Param("login") String login) throws NotFoundException {
        return repository.findByLogin(login).orElseThrow(() -> new NotFoundException());
    }

    @GetMapping("/search/findOneByResetKey")
    @PreAuthorize("hasAuthority('AuthAdmin')")
    public User findOneByResetKey(@Param("login") String login) throws NotFoundException {
        return repository.findOneByResetKey(login).orElseThrow(() -> new NotFoundException());
    }

    @GetMapping("/search/findByEmail")
    @PreAuthorize("hasAuthority('AuthAdmin')")
    public User findByEmail(@Param("login") String login) throws NotFoundException {
        return repository.findByEmail(login).orElseThrow(() -> new NotFoundException());
    }

    /**
     * The following methods should be moved for a user service if one is
     * created *
     */
    private boolean hasPermissionToGrantAuthorities(User userToBeCreated) {
        if (userToBeCreated.getGrantedAuthorities() == null || userToBeCreated.getGrantedAuthorities().isEmpty()) {
            return true;
        }
        String authUser = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User currentUser = repository.findByLogin(authUser).orElseThrow(() -> new AuthenticationCredentialsNotFoundException("no session"));
        int currentUserLevel = getHighestUserAuthority(currentUser.getUserId());
        int newUserWantedLevel = getHighestUserAuthority(userToBeCreated.getGrantedAuthorities().
                stream().map(grantedAuth -> grantedAuth.getAuthorityId()).collect(Collectors.toList()));
        return currentUserLevel >= newUserWantedLevel;

    }

    private int getHighestUserAuthority(Long userId) {
        Collection<Authority> userAuths = authorityRepo.findAuthoritiesByUserId(userId);
        return getHighestUserAuthority(userAuths);
    }

    private int getHighestUserAuthority(Collection<Authority> userAuths) {
        int max = -1;
        for (Authority auth : userAuths) {
            if (auth.getLevel() > max) {
                max = auth.getLevel();
            }
        }
        return max;
    }

    private Iterable<AuthProviderApplication> getActivePushableApps() {
        AuthProviderApplication activeAndPushable = new AuthProviderApplication();
        activeAndPushable.setActive(true);
        activeAndPushable.setPushUsers(true);
        return this.appsRepo.findAll(Example.of(activeAndPushable));
    }

}
