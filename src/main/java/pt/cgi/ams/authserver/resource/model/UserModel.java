package pt.cgi.ams.authserver.resource.model;

import java.time.Instant;
import java.util.Collection;
import java.util.stream.Collectors;
import javax.validation.constraints.NotNull;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import pt.cgi.ams.authserver.db.User;
import pt.cgi.ams.authserver.security.AuthProviderImpl;

/**
 *
 * @author silvagc
 */
public class UserModel {

    private static Logger LOG = LoggerFactory.getLogger(UserModel.class);
    private Long userId;

    @NotNull
    private String login;
    @NotNull
    private String firstName;
    private String lastName;
    @NotNull
    private String email;
    @NotNull
    private User.AuthType authTypeId;
    private boolean activated = false;

    private String langKey;

    private String imageUrl;

    private String activationKey;

    private String resetKey;

    private Instant resetDate = null;

    private Long managerId;
    private Float standardCost;

    private Collection<GrantedAuthority> grantedAuthorities;

    public UserModel() {

    }

    public UserModel(Long userId, String login, String firstName, String lastName, String email) {
        this.userId = userId;
        this.login = login;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
    }

    public Collection<GrantedAuthority> getGrantedAuthorities() {
        return grantedAuthorities;
    }

    public void setGrantedAuthorities(Collection<GrantedAuthority> grantedAuthorities) {
        this.grantedAuthorities = grantedAuthorities;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isActivated() {
        return activated;
    }

    public void setActivated(boolean activated) {
        this.activated = activated;
    }

    public String getLangKey() {
        return langKey;
    }

    public void setLangKey(String langKey) {
        this.langKey = langKey;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getActivationKey() {
        return activationKey;
    }

    public void setActivationKey(String activationKey) {
        this.activationKey = activationKey;
    }

    public String getResetKey() {
        return resetKey;
    }

    public void setResetKey(String resetKey) {
        this.resetKey = resetKey;
    }

    public Instant getResetDate() {
        return resetDate;
    }

    public void setResetDate(Instant resetDate) {
        this.resetDate = resetDate;
    }

    public User.AuthType getAuthTypeId() {
        return authTypeId;
    }

    public void setAuthTypeId(User.AuthType authTypeId) {
        this.authTypeId = authTypeId;
    }

    public Long getManagerId() {
        return managerId;
    }

    public void setManagerId(Long managerId) {
        this.managerId = managerId;
    }

    public Float getStandardCost() {
        return standardCost;
    }

    public void setStandardCost(Float standardCost) {
        this.standardCost = standardCost;
    }

    public User parse() {
        User user = new User();
        user.setUserId(getUserId());
        user.setActivated(true);
        user.setActivationKey(getActivationKey());
        user.setEmail(getEmail());
        user.setFirstName(getFirstName());
        user.setImageUrl(getImageUrl());
        user.setLangKey(getLangKey());
        user.setLastName(getLastName());
        user.setResetDate(getResetDate());
        user.setResetKey(getResetKey());
        user.setLogin(getLogin());

        if (user.getAuthTypeId() == null) {
            LOG.info("Submited user doesn't have an authType ");
            LOG.debug("Assuming the LOCAL authType ");
            user.setAuthTypeId(User.AuthType.LOCAL);
        } else {
            user.setAuthTypeId(getAuthTypeId());

        }
        return user;
    }

    public static UserModel build(User user) {
        UserModel model = new UserModel();
        model.setActivated(user.isActivated());
        model.setActivationKey(user.getActivationKey());
        model.setEmail(user.getEmail());
        model.setFirstName(user.getFirstName());
        model.setLastName(user.getLastName());
        model.setImageUrl(user.getImageUrl());
        model.setLangKey(user.getLangKey());
        model.setLogin(user.getLogin());
        model.setResetDate(user.getResetDate());
        model.setResetKey(user.getResetKey());
        model.setUserId(user.getUserId());
        model.setAuthTypeId(user.getAuthTypeId());
        if (user.getManager() != null) {
            model.setManagerId(user.getManager().getUserId());
        }
        model.setStandardCost(user.getStandardCost());
        return model;
    }

    /**
     * Uses current user authorities
     *
     * @param user
     * @return
     */
    public static UserModel buildWithSpringAuthorities(User user) {
        UserModel model = new UserModel();
        model.setActivated(user.isActivated());
        model.setActivationKey(user.getActivationKey());
        model.setEmail(user.getEmail());
        model.setFirstName(user.getFirstName());
        model.setLastName(user.getLastName());
        model.setImageUrl(user.getImageUrl());
        model.setLangKey(user.getLangKey());
        model.setLogin(user.getLogin());
        model.setResetDate(user.getResetDate());
        model.setResetKey(user.getResetKey());
        model.setUserId(user.getUserId());
        model.setGrantedAuthorities(AuthProviderImpl.getSpringAuthorities(user));
        model.setAuthTypeId(user.getAuthTypeId());
        if (user.getManager() != null) {
            model.setManagerId(user.getManager().getUserId());
        }
        model.setStandardCost(user.getStandardCost());
        return model;
    }

    /**
     * Uses database user authorities
     *
     * @param user
     * @return
     */
    public static UserModel buildWithAuthorities(User user) {
        UserModel model = new UserModel();
        model.setActivated(user.isActivated());
        model.setActivationKey(user.getActivationKey());
        model.setEmail(user.getEmail());
        model.setFirstName(user.getFirstName());
        model.setLastName(user.getLastName());
        model.setImageUrl(user.getImageUrl());
        model.setLangKey(user.getLangKey());
        model.setLogin(user.getLogin());
        model.setResetDate(user.getResetDate());
        model.setResetKey(user.getResetKey());
        model.setUserId(user.getUserId());
        if (user.getGrantedAuthorities() != null && !user.getGrantedAuthorities().isEmpty()) {
            model.setGrantedAuthorities(user.getGrantedAuthorities().stream().map(authority -> {
                return new SimpleGrantedAuthority(authority.getAuthorityId().getAuthorityName());
            }).collect(Collectors.toList()));
        }
        model.setAuthTypeId(user.getAuthTypeId());
        if (user.getManager() != null) {
            model.setManagerId(user.getManager().getUserId());
        }
        model.setStandardCost(user.getStandardCost());
        return model;
    }

    @Override
    public String toString() {
        return "UserModel{" + "userId=" + userId + ", login=" + login + ", firstName=" + firstName + ", lastName=" + lastName + ", email=" + email + ", authTypeId=" + authTypeId + ", activated=" + activated + ", langKey=" + langKey + ", imageUrl=" + imageUrl + ", activationKey=" + activationKey + ", resetKey=" + resetKey + ", resetDate=" + resetDate + ", managerId=" + managerId + ", standardCost=" + standardCost + ", grantedAuthorities=" + grantedAuthorities + '}';
    }

}
