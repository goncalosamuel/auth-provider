package pt.cgi.ams.authserver.exception;

import java.io.IOException;

/**
 *
 * @author silvagc
 */
public class RemoteServiceException extends IOException{

    public RemoteServiceException() {
    }

    public RemoteServiceException(String message) {
        super(message);
    }

    public RemoteServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public RemoteServiceException(Throwable cause) {
        super(cause);
    }

  
    
    
    
}
