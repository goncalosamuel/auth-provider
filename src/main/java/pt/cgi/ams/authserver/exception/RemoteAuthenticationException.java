package pt.cgi.ams.authserver.exception;

/**
 *
 * @author silvagc
 */
public class RemoteAuthenticationException extends RemoteServiceException {

    public RemoteAuthenticationException() {
    }

    public RemoteAuthenticationException(String message) {
        super(message);
    }

    public RemoteAuthenticationException(String message, Throwable cause) {
        super(message, cause);
    }

    public RemoteAuthenticationException(Throwable cause) {
        super(cause);
    }
    
}
