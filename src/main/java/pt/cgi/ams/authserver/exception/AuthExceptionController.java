package pt.cgi.ams.authserver.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 *
 * @author tiagomartinho.soares
 */
@ControllerAdvice
public class AuthExceptionController extends ResponseEntityExceptionHandler {

    /**
     * Handler for 500 exceptions
     *
     * @param e
     * @return Response entity with error code 500
     */
    @ExceptionHandler({AuthServerException.class})
    public ResponseEntity<?> handleErroInterno(AuthServerException e) {
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
    }

    /**
     * Handler for 404 exceptions
     *
     * @param e
     * @return Response entity with error code 404
     */
    @ExceptionHandler({UserNotFoundException.class, NotFoundException.class})
    public ResponseEntity<?> handleNaoEncontrado(AuthServerException e) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
    }

    /**
     * Handler for 409 conflit
     *
     * @param e
     * @return Response entity with error code 404
     */
    @ExceptionHandler({LoginAlreadyExistsException.class})
    public ResponseEntity<?> handleConflito(AuthServerException e) {
        return ResponseEntity.status(HttpStatus.CONFLICT).body(e.getMessage());
    }

    /**
     * Handler for ADException exceptions
     *
     * @param e
     * @return Response entity with error code 500
     */
    @ExceptionHandler({ADException.class})
    public ResponseEntity<?> handleProibido(ADException e) {
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
    }

}
