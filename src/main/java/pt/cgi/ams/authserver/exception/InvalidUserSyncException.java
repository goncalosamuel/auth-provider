package pt.cgi.ams.authserver.exception;

/**
 *
 * @author silvagc
 */
public class InvalidUserSyncException extends UserSyncException{

    public InvalidUserSyncException() {
    }

    public InvalidUserSyncException(String message) {
        super(message);
    }

    public InvalidUserSyncException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidUserSyncException(Throwable cause) {
        super(cause);
    }

    public InvalidUserSyncException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
    
}
