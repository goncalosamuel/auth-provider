package pt.cgi.ams.authserver.exception;

import org.springframework.http.HttpStatus;

public class InvalidPasswordException extends AuthServerException {

    public InvalidPasswordException() {
        super("Incorrect password");
    }
}