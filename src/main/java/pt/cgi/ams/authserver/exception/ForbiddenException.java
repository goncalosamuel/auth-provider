package pt.cgi.ams.authserver.exception;

/**
 *
 * @author silvagc
 */
public class ForbiddenException extends AuthServerException{

    public ForbiddenException() {
    }

    public ForbiddenException(String message) {
        super(message);
    }

    public ForbiddenException(String message, Throwable cause) {
        super(message, cause);
    }

    public ForbiddenException(Throwable cause) {
        super(cause);
    }
    
}
