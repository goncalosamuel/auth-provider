package pt.cgi.ams.authserver.exception;

import org.springframework.security.core.AuthenticationException;

/**
 *
 * @author silvagc
 */
public class ADException extends AuthenticationException {

    public ADException(String msg, Throwable t) {
        super(msg, t);
    }

    public ADException(String msg) {
        super(msg);
    }

}
