package pt.cgi.ams.authserver.exception;

/**
 *
 * @author silvagc
 */
public class UserSyncException extends AuthServerException {

    public UserSyncException() {
    }

    public UserSyncException(String message) {
        super(message);
    }

    public UserSyncException(String message, Throwable cause) {
        super(message, cause);
    }

    public UserSyncException(Throwable cause) {
        super(cause);
    }

    public UserSyncException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
    
}
