package pt.cgi.ams.authserver.exception;

import java.util.function.Consumer;
import pt.cgi.ams.authserver.Throwing;

/**
 *
 * @author silvagc
 */
@FunctionalInterface
public interface ThrowingConsumer<T> extends Consumer<T> {

    default public void accept(T t){
          try {
            accept0(t);
        } catch (Throwable ex) {
            System.out.println("here");
            Throwing.sneakyThrow(ex);
        }
    };
    void accept0(T e) throws Throwable;
    
}
