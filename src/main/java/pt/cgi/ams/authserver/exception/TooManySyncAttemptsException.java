
package pt.cgi.ams.authserver.exception;

/**
 *
 * @author silvagc
 */
public class TooManySyncAttemptsException extends UserSyncException {

    public TooManySyncAttemptsException() {
    }

    public TooManySyncAttemptsException(String message) {
        super(message);
    }

    public TooManySyncAttemptsException(String message, Throwable cause) {
        super(message, cause);
    }

    public TooManySyncAttemptsException(Throwable cause) {
        super(cause);
    }

}
