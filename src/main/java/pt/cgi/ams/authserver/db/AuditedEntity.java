package pt.cgi.ams.authserver.db;

import java.io.Serializable;
import java.util.Calendar;
import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

/**
 *
 * @author silvagc
 */

/**
 *
 * @author silvagc
 */
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public class AuditedEntity  implements Serializable{

    @Column(name = "creation_date")
    @CreatedDate
    private Calendar createdDate;
    
    @Column(name = "modification_date")
    @LastModifiedDate
    private Calendar modifiedDate;
    
    
    @Column(name = "create_by")
    @CreatedBy
    private Integer createdBy;

    @Column(name = "modified_by")
    @LastModifiedBy
    private Integer modifiedBy;

    public Calendar getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Calendar createdDate) {
        this.createdDate = createdDate;
    }

    public Calendar getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Calendar modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(Integer modifiedBy) {
        this.modifiedBy = modifiedBy;
    }
    
    
}
