package pt.cgi.ams.authserver.db;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author silvagc
 */
@Entity
@Table(name = "auth_provider_authority")
public class Authority implements org.springframework.security.core.GrantedAuthority {

    private static final long serialVersionUID = -8976469574510718181L;
    @Id
    @Column(name = "authority_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long authorityId;
    @Column(name = "authority_name", nullable = false, unique = true)
    private String authorityName;

    /* zero is the default value */
    @Column(name = "authority_level", nullable = false)
    private Integer level = 0;

    @OneToMany(mappedBy = "authorityId", fetch = FetchType.LAZY)
    @JsonIgnore
    private List<GrantedAuthority> grantedAuthorities;

    public List<GrantedAuthority> getGrantedAuthorities() {
        return grantedAuthorities;
    }

    public void setGrantedAuthorities(List<GrantedAuthority> grantedAuthorities) {
        this.grantedAuthorities = grantedAuthorities;
    }

    public Long getAuthorityId() {
        return authorityId;
    }

    public void setAuthorityId(Long authorityId) {
        this.authorityId = authorityId;
    }

    public String getAuthorityName() {
        return authorityName;
    }

    public void setAuthorityName(String authorityName) {
        this.authorityName = authorityName;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    
    @Override
    public String getAuthority() {
        return getAuthorityName();
    }

}
