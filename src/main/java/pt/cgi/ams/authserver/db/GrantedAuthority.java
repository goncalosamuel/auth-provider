package pt.cgi.ams.authserver.db;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;


/**
 *
 * @author silvagc
 */
@Entity
@Table(name = "auth_provider_granted_authority",
uniqueConstraints = {
        @UniqueConstraint(columnNames = {"user_id", "authority_id"})
})
public class GrantedAuthority extends AuditedEntity  {

    private static final long serialVersionUID = 13231234412L;
    @Id
    @Column(name = "granted_authority_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long grantedAuthorityId;

    @JoinColumn(name = "user_id", referencedColumnName = "user_id", nullable = false)
    @ManyToOne(fetch = FetchType.EAGER)
    @JsonBackReference
    private User userId;

    @JoinColumn(name = "authority_id", referencedColumnName = "authority_id", nullable = false)
    @ManyToOne(fetch = FetchType.EAGER)
    private Authority authorityId;
     

    public Long getGrantedAuthorityId() {
        return grantedAuthorityId;
    }

    public void setGrantedAuthorityId(Long grantedAuthorityId) {
        this.grantedAuthorityId = grantedAuthorityId;
    }

    public User getUserId() {
        return userId;
    }

    public void setUserId(User userId) {
        this.userId = userId;
    }

    public Authority getAuthorityId() {
        return authorityId;
    }

    public void setAuthorityId(Authority authorityId) {
        this.authorityId = authorityId;
    }

   

}
