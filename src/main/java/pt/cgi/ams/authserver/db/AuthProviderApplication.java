package pt.cgi.ams.authserver.db;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author tiagomartinho.soares
 */
@Entity
@Table(name = "auth_provider_application")
public class AuthProviderApplication {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "application_id")
    private Long applicationId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "application_name")
    private String applicationName;
    @Size(max = 255)
    @Column(name = "application_description")
    private String applicationDescription;
    @Size(max = 255)
    @Column(name = "application_url")
    private String applicationUrl;

    @Size(max = 255)
    @Column(name = "user_service_url")
    private String userServiceUrl;

    @Basic(optional = false)
    @NotNull
    @Column(name = "push_users")
    private boolean pushUsers;

    @Basic(optional = false)
    @NotNull
    @Column(name = "active")
    private boolean active;

    public AuthProviderApplication() {
    }

    public AuthProviderApplication(Long applicationId) {
        this.applicationId = applicationId;
    }

    public AuthProviderApplication(Long applicationId, String applicationName, boolean active) {
        this.applicationId = applicationId;
        this.applicationName = applicationName;
        this.active = active;
    }

    public Long getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(Long applicationId) {
        this.applicationId = applicationId;
    }

    public String getApplicationName() {
        return applicationName;
    }

    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }

    public String getApplicationDescription() {
        return applicationDescription;
    }

    public void setApplicationDescription(String applicationDescription) {
        this.applicationDescription = applicationDescription;
    }

    public String getApplicationUrl() {
        return applicationUrl;
    }

    public void setApplicationUrl(String applicationUrl) {
        this.applicationUrl = applicationUrl;
    }

    public boolean getActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getUserServiceUrl() {
        return userServiceUrl;
    }

    public void setUserServiceUrl(String UserServiceUrl) {
        this.userServiceUrl = UserServiceUrl;
    }

    public boolean isPushUsers() {
        return pushUsers;
    }

    public void setPushUsers(boolean pushUsers) {
        this.pushUsers = pushUsers;
    }
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (applicationId != null ? applicationId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AuthProviderApplication)) {
            return false;
        }
        AuthProviderApplication other = (AuthProviderApplication) object;
        if ((this.applicationId == null && other.applicationId != null) || (this.applicationId != null && !this.applicationId.equals(other.applicationId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pt.cgi.ams.authserver.db.AuthProviderApplication[ applicationId=" + applicationId + " ]";
    }

}
