package pt.cgi.ams.authserver.db;

import com.fasterxml.jackson.annotation.JsonBackReference;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author silvagc
 */
@Entity
@Table(name = "auth_provider_user_synch")
public class UserSynchronization {

    @Id
    @Column(name = "user_sync_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long userSynchronizationId;
    @NotNull
    @Column(name = "completed", nullable = false)
    private boolean completed;
    @Column(name = "fail_message", nullable = true, length = 2000)
    private String failMessage;
    @Column(name = "last_sync_date", nullable = true)
    private Date lastSyncDate;
    @Column(name = "number_of_tries", nullable = true)
    private Integer numberOfTries;

    @ManyToOne(fetch = FetchType.EAGER)
    @JsonBackReference
    @JoinColumn(name = "application_id", referencedColumnName = "application_id", nullable = false)
    private AuthProviderApplication applicationId;
    @ManyToOne(fetch = FetchType.LAZY)
    @JsonBackReference
    @JoinColumn(name = "user_id", referencedColumnName = "user_id", nullable = false)
    private User user;

    public Integer getNumberOfTries() {
        return numberOfTries;
    }

    public void setNumberOfTries(Integer numberOfTries) {
        this.numberOfTries = numberOfTries;
    }

    
    public Long getUserSynchronizationId() {
        return userSynchronizationId;
    }

    public void setUserSynchronizationId(Long userSynchronizationId) {
        this.userSynchronizationId = userSynchronizationId;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    public String getFailMessage() {
        return failMessage;
    }

    public void setFailMessage(String failMessage) {
        this.failMessage = failMessage;
    }

    public Date getLastSyncDate() {
        return lastSyncDate;
    }

    public void setLastSyncDate(Date lastSyncDate) {
        this.lastSyncDate = lastSyncDate;
    }

    public AuthProviderApplication getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(AuthProviderApplication applicationId) {
        this.applicationId = applicationId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

}
