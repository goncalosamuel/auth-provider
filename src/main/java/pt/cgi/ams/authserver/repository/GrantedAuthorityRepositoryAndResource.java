package pt.cgi.ams.authserver.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.security.access.prepost.PreAuthorize;
import pt.cgi.ams.authserver.db.GrantedAuthority;

import java.util.Collection;

@RepositoryRestResource(collectionResourceRel = "grantedAuthorities", path="grantedAuthority")
public interface GrantedAuthorityRepositoryAndResource extends PagingAndSortingRepository<GrantedAuthority, Long> {

    @PreAuthorize("permitAll()")
    @Query("SELECT ga from GrantedAuthority ga " +
            "INNER JOIN Authority auth ON ga.authorityId.authorityId = auth.authorityId " +
            "AND ga.userId.userId = :userId ")
    public Collection<GrantedAuthority> findGrantedAuthotitiesByUserId(@Param("userId") Long userId);

    @PreAuthorize("permitAll()")
    @Query("SELECT ga from GrantedAuthority ga " +
            "where ga.authorityId.authorityId = :authorityId " +
            "and ga.userId.userId = :userId ")
    public GrantedAuthority getByUserAndAuthority(@Param("userId") Long userId,
                                                  @Param("authorityId")Long authorityId);

}
