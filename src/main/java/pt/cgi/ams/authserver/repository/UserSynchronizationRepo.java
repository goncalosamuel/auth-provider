package pt.cgi.ams.authserver.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pt.cgi.ams.authserver.db.UserSynchronization;

/**
 *
 * @author silvagc
 */
public interface UserSynchronizationRepo extends JpaRepository<UserSynchronization, Long>{
    
}
