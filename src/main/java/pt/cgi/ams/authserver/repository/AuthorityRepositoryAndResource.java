package pt.cgi.ams.authserver.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.security.access.prepost.PreAuthorize;
import pt.cgi.ams.authserver.db.Authority;

import java.util.Collection;
import java.util.List;

@RepositoryRestResource(collectionResourceRel = "authorities", path = "authority")
//@PreAuthorize("hasAnyAuthority('AuthAdmin')")
public interface AuthorityRepositoryAndResource extends PagingAndSortingRepository<Authority, Long> {

    
    public Authority findOneByAuthorityNameIgnoreCase(@Param("name") String authorityName);

    @PreAuthorize("permitAll()")
    @Query("SELECT auth from Authority auth " +
            "INNER JOIN GrantedAuthority ga ON auth.authorityId = ga.authorityId.authorityId " +
            "AND ga.userId.userId = :userId ")
    public Collection<Authority> findAuthoritiesByUserId(@Param("userId") Long userId);

    @PreAuthorize("permitAll()")
    @Query("SELECT auth from Authority auth " +
            "INNER JOIN GrantedAuthority ga ON auth.authorityId = ga.authorityId.authorityId " +
            "AND ga.userId.userId = :userId " +
            "WHERE auth.authorityId = :authorityId")
    public Authority getUserAuthority(@Param("authorityId") Long authorityId,@Param("userId") Long userId);
    
    @Query("SELECT auth from Authority auth")
    public Collection<Authority> findAllAuthorities();


}
