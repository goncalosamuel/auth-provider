package pt.cgi.ams.authserver.repository;

import java.util.Collection;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.repository.query.QueryByExampleExecutor;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import pt.cgi.ams.authserver.db.AuthProviderApplication;

/**
 *
 * @author tiagomartinho.soares
 */

@RepositoryRestResource(collectionResourceRel = "applications", path="application")
public interface ApplicationRepositoryAndResource extends PagingAndSortingRepository<AuthProviderApplication, Long>, QueryByExampleExecutor<AuthProviderApplication> {
    
    public AuthProviderApplication findByApplicationId(@Param("applicationId") Long applicationId);
    
    @Query("SELECT a from AuthProviderApplication a WHERE a.active = true")
    public Collection<AuthProviderApplication> findActiveApps();
}
