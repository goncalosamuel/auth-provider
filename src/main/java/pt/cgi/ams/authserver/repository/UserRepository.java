package pt.cgi.ams.authserver.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import pt.cgi.ams.authserver.db.User;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.security.access.prepost.PreAuthorize;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;


/**
 *
 * @author silvagc
 */

public interface UserRepository extends JpaRepository<User, Long>{
    
    
    
    public Optional<User> findByLogin(@Param("login") String login);

    public Optional<User> findByEmail(@Param("email") String email);

    
    public Page<User> findByLoginContainingIgnoreCase(@Param("login") String login, Pageable pageable);

    public Optional<User> findOneByResetKey(String key);

    

}
