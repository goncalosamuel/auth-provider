package pt.cgi.ams.authserver;

import java.util.function.Consumer;
import javax.validation.constraints.NotNull;
import pt.cgi.ams.authserver.exception.ThrowingConsumer;

/**
 *
 * @author silvagc
 */
public class Throwing {
     private Throwing() {}

    public static <T,E extends Exception> Consumer<T> rethrow(@NotNull final ThrowingConsumer<T> consumer) throws E {
        return consumer;
    }

    /**
     * The compiler sees the signature with the throws T inferred to a RuntimeException type, so it
     * allows the unchecked exception to propagate.
     * 
     * http://www.baeldung.com/java-sneaky-throws
     */
    @SuppressWarnings("unchecked")
    public static <E extends Exception> void sneakyThrow(Throwable ex) throws E {
        throw (E) ex;
    }
}
