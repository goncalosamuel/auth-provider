INSERT INTO auth_provider_user(login, password_hash, first_name, last_name, email, image_url, activated, lang_key, activation_key, reset_key, auth_type_id) VALUES ('carlosLima', '$2a$10$gSAhZrxMllrbgj/kkK9UceBPpChGWJA7SYIb1Mqo.n5aNLq1/oRrC', 'Carlos', 'Lima', 'fornecedor@teste.com', null, true, 'pt-pt', null, null,1);
INSERT INTO auth_provider_user(login, password_hash, first_name, last_name, email, image_url, activated, lang_key, activation_key, reset_key, auth_type_id) VALUES ('teste', '$2a$10$2UTqjIha3WumooGvQaFQm.7U7bINyjBXuAu42H5wqFP2MpfVBzxp.', 'teste', 'teste', 't.test@teste.com', null, true, 'pt-pt', null, null,1);
INSERT INTO auth_provider_user(login, password_hash, first_name, last_name, email, image_url, activated, lang_key, activation_key, reset_key, auth_type_id) VALUES ('silvagc', '$2a$10$gSAhZrxMllrbgj/kkK9UceBPpChGWJA7SYIb1Mqo.n5aNLq1/oRrC', 'Gonçalo', '123', 'go.silva@cgi.com', null, true, 'pt-pt', null, null,1);
INSERT INTO auth_provider_user(login, password_hash, first_name, last_name, email, image_url, activated, lang_key, activation_key, reset_key, auth_type_id) VALUES ('tiagomartinho.soares', '$2a$10$gSAhZrxMllrbgj/kkK9UceBPpChGWJA7SYIb1Mqo.n5aNLq1/oRrC', 'Tiago', '123', 'tiagomartinho.soares@cgi.com', null, true, 'pt-pt', null, null,0);
insert into auth_provider_authority(authority_name) values('AuthAdmin');
insert into auth_provider_authority(authority_name) values('Admin');
insert into auth_provider_authority(authority_name) values('Manager');
insert into auth_provider_authority(authority_name) values('StreamManager');
insert into auth_provider_authority(authority_name) values('ProjectManager');
insert into auth_provider_authority(authority_name) values('Outsourced');
insert into auth_provider_authority(authority_name) values('Member');
insert into auth_provider_authority(authority_name) values('SupportAdmin');
insert into auth_provider_authority(authority_name) values('BusinessSupport');

          
INSERT INTO auth_provider_granted_authority (authority_id, user_id) values(1,3);
INSERT INTO auth_provider_granted_authority (authority_id, user_id) values(2,3);
INSERT INTO auth_provider_granted_authority (authority_id, user_id) values(1,4);
INSERT INTO auth_provider_granted_authority (authority_id, user_id) values(2,4);

INSERT INTO auth_provider_application (application_id, application_name, active, application_description, application_url) values(1, 'Portal AMS', true, 'DESCRIPTION AMS', 'http://google.pt');
INSERT INTO auth_provider_application (application_id, application_name, active, application_description, application_url) values(2, 'Portal IT', true, 'DESCRIPTION IT', 'http://sapo.pt');

commit;

